﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegLangDiplom
{
    class TNKA : TKA
    {
        public TNKA(int countStates, string alphabet) : base(countStates, alphabet) {
            type = TType.NKA;
        }

        public override string getTypeString()
        {
            return "НКА";
        }

        public override string CheckLine(string chain)
        {
            int q = StartState;
            List<int> memq = new List<int>();
            List<string> mems = new List<string>();
            int num = -1;
            string outstr = getKonf(q, chain);
            do {
                if (num >= 0)
                {
                    q = memq[num];
                    chain = mems[num];
                    outstr += Environment.NewLine + "[" + num.ToString() + "]" + getKonf(q, chain);
                }
                while (chain.Length > 0)
                {
                    int index = Alphabet.IndexOf(chain[0]);
                    if (index < 0 || Table[q, index].Count == 0) break;
                    for (int i = 1; i < Table[q, index].Count; i++)
                    {
                        outstr += "[" + memq.Count.ToString() + "]";
                        memq.Add(Table[q, index][i]);
                        mems.Add(chain.Remove(0, 1));
                    }
                    q = Table[q, index].First();
                    chain = chain.Remove(0, 1);
                    outstr += "├" + getKonf(q, chain);
                }
                if (chain.Length == 0 && FinishStates.Contains(q))
                {
                    outstr += "├STOP(+)";
                    break;
                }
                else
                {
                    if (chain.Length > 0)
                        outstr += "├STOP(-) Нет подходящего правила перехода";
                    else
                        if (!FinishStates.Contains(q)) 
                            outstr += "├STOP(-) Автомат не пришел в конечное состояние";
                    num++;
                }
            } while (memq.Count > num);
            return outstr + Environment.NewLine;
        }

        public TDKA ConvertToDKA()
        {
            List<int> states = new List<int>();
            states.Add(1 << StartState);
            List<int>[] rules = new List<int>[Alphabet.Length];
            for (int i = 0; i < Alphabet.Length; i++) rules[i] = new List<int>();
            int num = 0;
            while (num < states.Count)
            {
                int curStates = states[num];
                for (int i = 0; i < Alphabet.Length; i++)
                {
                    int nextState = 0;
                    for (int j = 0; (curStates >> j) > 0; j++)
                    {
                        int state = (curStates >> j) & 1;
                        if (state == 1)
                            foreach (int k in Table[j, i]) nextState = nextState | (1 << k);
                    }
                    rules[i].Add(nextState);
                    if (nextState > 0 && !states.Contains(nextState)) states.Add(nextState);
                }
                num++;
            }

            TDKA DKA = new TDKA(states.Count, Alphabet);
            DKA.setName("ДКА из " + Name);
            DKA.setStartState(0);
            for (int i = 0; i < Alphabet.Length; i++)
            {
                int curState = 0;
                foreach (int k in rules[i])
                {
                    if (k > 0)
                        DKA.addRule(curState, states.IndexOf(k), Alphabet[i]);
                    curState++;
                }
            }
            List<int> finish = new List<int>();
            int index = 0;
            foreach (int i in states)
            {
                if (FinishStates.Exists(x => ((i >> x) & 1) == 1)) finish.Add(index);
                index++;
            }
            DKA.setFinishStates(finish);
            return DKA;
        }
    }
}
