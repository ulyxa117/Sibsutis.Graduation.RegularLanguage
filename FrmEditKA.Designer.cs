﻿namespace RegLangDiplom
{
    partial class FrmEditKA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCountStates = new System.Windows.Forms.Label();
            this.lblStartState = new System.Windows.Forms.Label();
            this.lblFinishStates = new System.Windows.Forms.Label();
            this.lblTable = new System.Windows.Forms.Label();
            this.lblq0 = new System.Windows.Forms.Label();
            this.lblc = new System.Windows.Forms.Label();
            this.lblq1 = new System.Windows.Forms.Label();
            this.numCountStates = new System.Windows.Forms.NumericUpDown();
            this.numStartState = new System.Windows.Forms.NumericUpDown();
            this.txtFinishStates = new System.Windows.Forms.TextBox();
            this.cbq1 = new System.Windows.Forms.ComboBox();
            this.cbq0 = new System.Windows.Forms.ComboBox();
            this.cbc = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbNKA = new System.Windows.Forms.RadioButton();
            this.rbDKA = new System.Windows.Forms.RadioButton();
            this.txtAlphabet = new System.Windows.Forms.TextBox();
            this.lblAlphabet = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lstRules = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.numCountStates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartState)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCountStates
            // 
            this.lblCountStates.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCountStates.Location = new System.Drawing.Point(13, 9);
            this.lblCountStates.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCountStates.Name = "lblCountStates";
            this.lblCountStates.Size = new System.Drawing.Size(169, 22);
            this.lblCountStates.TabIndex = 1;
            this.lblCountStates.Text = "Количество состояний";
            this.lblCountStates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStartState
            // 
            this.lblStartState.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStartState.Location = new System.Drawing.Point(13, 36);
            this.lblStartState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStartState.Name = "lblStartState";
            this.lblStartState.Size = new System.Drawing.Size(102, 22);
            this.lblStartState.TabIndex = 2;
            this.lblStartState.Text = "Начальное состояние";
            this.lblStartState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFinishStates
            // 
            this.lblFinishStates.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFinishStates.Location = new System.Drawing.Point(13, 65);
            this.lblFinishStates.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFinishStates.Name = "lblFinishStates";
            this.lblFinishStates.Size = new System.Drawing.Size(102, 22);
            this.lblFinishStates.TabIndex = 3;
            this.lblFinishStates.Text = "Конечные состояния";
            this.lblFinishStates.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTable
            // 
            this.lblTable.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTable.Location = new System.Drawing.Point(13, 141);
            this.lblTable.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTable.Name = "lblTable";
            this.lblTable.Size = new System.Drawing.Size(252, 22);
            this.lblTable.TabIndex = 4;
            this.lblTable.Text = "Таблица переходов {q0, c, q1}";
            this.lblTable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblq0
            // 
            this.lblq0.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblq0.Location = new System.Drawing.Point(13, 166);
            this.lblq0.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblq0.Name = "lblq0";
            this.lblq0.Size = new System.Drawing.Size(32, 22);
            this.lblq0.TabIndex = 5;
            this.lblq0.Text = "q0";
            this.lblq0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblc
            // 
            this.lblc.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblc.Location = new System.Drawing.Point(87, 166);
            this.lblc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblc.Name = "lblc";
            this.lblc.Size = new System.Drawing.Size(28, 22);
            this.lblc.TabIndex = 6;
            this.lblc.Text = "c";
            this.lblc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblq1
            // 
            this.lblq1.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblq1.Location = new System.Drawing.Point(161, 166);
            this.lblq1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblq1.Name = "lblq1";
            this.lblq1.Size = new System.Drawing.Size(32, 22);
            this.lblq1.TabIndex = 7;
            this.lblq1.Text = "q1";
            this.lblq1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numCountStates
            // 
            this.numCountStates.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numCountStates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numCountStates.Location = new System.Drawing.Point(190, 9);
            this.numCountStates.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numCountStates.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCountStates.Name = "numCountStates";
            this.numCountStates.Size = new System.Drawing.Size(60, 22);
            this.numCountStates.TabIndex = 8;
            this.numCountStates.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCountStates.ValueChanged += new System.EventHandler(this.numCountStates_ValueChanged);
            // 
            // numStartState
            // 
            this.numStartState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numStartState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numStartState.Location = new System.Drawing.Point(111, 34);
            this.numStartState.Name = "numStartState";
            this.numStartState.Size = new System.Drawing.Size(60, 22);
            this.numStartState.TabIndex = 9;
            // 
            // txtFinishStates
            // 
            this.txtFinishStates.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtFinishStates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFinishStates.Location = new System.Drawing.Point(111, 62);
            this.txtFinishStates.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFinishStates.MaxLength = 32000;
            this.txtFinishStates.Name = "txtFinishStates";
            this.txtFinishStates.Size = new System.Drawing.Size(139, 22);
            this.txtFinishStates.TabIndex = 10;
            // 
            // cbq1
            // 
            this.cbq1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbq1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbq1.FormattingEnabled = true;
            this.cbq1.Location = new System.Drawing.Point(190, 166);
            this.cbq1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbq1.Name = "cbq1";
            this.cbq1.Size = new System.Drawing.Size(41, 24);
            this.cbq1.TabIndex = 11;
            // 
            // cbq0
            // 
            this.cbq0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbq0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbq0.FormattingEnabled = true;
            this.cbq0.Location = new System.Drawing.Point(40, 166);
            this.cbq0.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbq0.Name = "cbq0";
            this.cbq0.Size = new System.Drawing.Size(41, 24);
            this.cbq0.TabIndex = 12;
            // 
            // cbc
            // 
            this.cbc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbc.FormattingEnabled = true;
            this.cbc.Location = new System.Drawing.Point(114, 166);
            this.cbc.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbc.MaxLength = 1;
            this.cbc.Name = "cbc";
            this.cbc.Size = new System.Drawing.Size(41, 24);
            this.cbc.TabIndex = 13;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Controls.Add(this.rbNKA);
            this.panel1.Controls.Add(this.rbDKA);
            this.panel1.Controls.Add(this.txtAlphabet);
            this.panel1.Controls.Add(this.lblAlphabet);
            this.panel1.Controls.Add(this.numCountStates);
            this.panel1.Controls.Add(this.txtFinishStates);
            this.panel1.Controls.Add(this.numStartState);
            this.panel1.Controls.Add(this.lblCountStates);
            this.panel1.Controls.Add(this.lblStartState);
            this.panel1.Controls.Add(this.lblFinishStates);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(253, 138);
            this.panel1.TabIndex = 14;
            // 
            // rbNKA
            // 
            this.rbNKA.AutoSize = true;
            this.rbNKA.Location = new System.Drawing.Point(72, 117);
            this.rbNKA.Name = "rbNKA";
            this.rbNKA.Size = new System.Drawing.Size(50, 20);
            this.rbNKA.TabIndex = 14;
            this.rbNKA.Text = "НКА";
            this.rbNKA.UseVisualStyleBackColor = true;
            // 
            // rbDKA
            // 
            this.rbDKA.AutoSize = true;
            this.rbDKA.Checked = true;
            this.rbDKA.Location = new System.Drawing.Point(16, 117);
            this.rbDKA.Name = "rbDKA";
            this.rbDKA.Size = new System.Drawing.Size(50, 20);
            this.rbDKA.TabIndex = 13;
            this.rbDKA.TabStop = true;
            this.rbDKA.Text = "ДКА";
            this.rbDKA.UseVisualStyleBackColor = true;
            // 
            // txtAlphabet
            // 
            this.txtAlphabet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtAlphabet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAlphabet.Location = new System.Drawing.Point(111, 92);
            this.txtAlphabet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAlphabet.MaxLength = 32000;
            this.txtAlphabet.Name = "txtAlphabet";
            this.txtAlphabet.Size = new System.Drawing.Size(139, 22);
            this.txtAlphabet.TabIndex = 12;
            this.txtAlphabet.TextChanged += new System.EventHandler(this.txtAlphabet_TextChanged);
            // 
            // lblAlphabet
            // 
            this.lblAlphabet.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblAlphabet.Location = new System.Drawing.Point(13, 91);
            this.lblAlphabet.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAlphabet.Name = "lblAlphabet";
            this.lblAlphabet.Size = new System.Drawing.Size(102, 22);
            this.lblAlphabet.TabIndex = 11;
            this.lblAlphabet.Text = "Алфавит";
            this.lblAlphabet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Location = new System.Drawing.Point(0, 197);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(253, 26);
            this.btnAdd.TabIndex = 15;
            this.btnAdd.Text = "Добавить правило перехода";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(125, 367);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 26);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(12, 367);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(113, 26);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lstRules
            // 
            this.lstRules.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstRules.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstRules.FormattingEnabled = true;
            this.lstRules.ItemHeight = 16;
            this.lstRules.Location = new System.Drawing.Point(12, 230);
            this.lstRules.Name = "lstRules";
            this.lstRules.Size = new System.Drawing.Size(228, 130);
            this.lstRules.TabIndex = 16;
            this.lstRules.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstRules_KeyDown);
            // 
            // FrmEditKA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(252, 400);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lstRules);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.cbc);
            this.Controls.Add(this.cbq0);
            this.Controls.Add(this.cbq1);
            this.Controls.Add(this.lblq1);
            this.Controls.Add(this.lblc);
            this.Controls.Add(this.lblq0);
            this.Controls.Add(this.lblTable);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(268, 439);
            this.MinimumSize = new System.Drawing.Size(268, 439);
            this.Name = "FrmEditKA";
            this.ShowIcon = false;
            this.Text = "Конечный автомат";
            this.Load += new System.EventHandler(this.FrmEditKA_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numCountStates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStartState)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCountStates;
        private System.Windows.Forms.Label lblStartState;
        private System.Windows.Forms.Label lblFinishStates;
        private System.Windows.Forms.Label lblTable;
        private System.Windows.Forms.Label lblq0;
        private System.Windows.Forms.Label lblc;
        private System.Windows.Forms.Label lblq1;
        private System.Windows.Forms.NumericUpDown numCountStates;
        private System.Windows.Forms.NumericUpDown numStartState;
        private System.Windows.Forms.TextBox txtFinishStates;
        private System.Windows.Forms.ComboBox cbq1;
        private System.Windows.Forms.ComboBox cbq0;
        private System.Windows.Forms.ComboBox cbc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ListBox lstRules;
        private System.Windows.Forms.TextBox txtAlphabet;
        private System.Windows.Forms.Label lblAlphabet;
        private System.Windows.Forms.RadioButton rbNKA;
        private System.Windows.Forms.RadioButton rbDKA;
    }
}