﻿namespace RegLangDiplom
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConstructionExample = new System.Windows.Forms.Button();
            this.panelRegularEvent = new System.Windows.Forms.Panel();
            this.btnRegularDKA = new System.Windows.Forms.Button();
            this.btnRegularNKA = new System.Windows.Forms.Button();
            this.btnRegularRemove = new System.Windows.Forms.Button();
            this.btnRegularEdit = new System.Windows.Forms.Button();
            this.panelList = new System.Windows.Forms.Panel();
            this.panelRegular = new System.Windows.Forms.Panel();
            this.lblGrammatic = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnStopGenerating = new System.Windows.Forms.Button();
            this.lblCountChains = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnGenerateChains = new System.Windows.Forms.Button();
            this.numMaxLength = new System.Windows.Forms.NumericUpDown();
            this.lblRegLengthMax = new System.Windows.Forms.Label();
            this.numMinLength = new System.Windows.Forms.NumericUpDown();
            this.lblRegLengthMin = new System.Windows.Forms.Label();
            this.lblHeaderGenerationChains = new System.Windows.Forms.Label();
            this.lstChains = new System.Windows.Forms.ListBox();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.btnCopy = new System.Windows.Forms.Button();
            this.txtNameConstruction = new System.Windows.Forms.TextBox();
            this.lblTypeConstruction = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.lblHeaderConstruction = new System.Windows.Forms.Label();
            this.panelKAEvent = new System.Windows.Forms.Panel();
            this.btnBuildRG = new System.Windows.Forms.Button();
            this.btnBuildDKAfromNKA = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnRemoveKA = new System.Windows.Forms.Button();
            this.btnEditKA = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.регулярнуюГрамматикуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.конечныйАвтоматToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отображатьГрафКАToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelKA = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtKALog = new System.Windows.Forms.TextBox();
            this.lblKA = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCheck = new System.Windows.Forms.Button();
            this.txtChain = new System.Windows.Forms.TextBox();
            this.lblKAHeader = new System.Windows.Forms.Label();
            this.panelRegularEvent.SuspendLayout();
            this.panelRegular.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinLength)).BeginInit();
            this.panelHeader.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelKAEvent.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelKA.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConstructionExample
            // 
            this.btnConstructionExample.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnConstructionExample.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnConstructionExample.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnConstructionExample.FlatAppearance.BorderSize = 0;
            this.btnConstructionExample.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnConstructionExample.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnConstructionExample.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConstructionExample.Location = new System.Drawing.Point(0, 24);
            this.btnConstructionExample.Margin = new System.Windows.Forms.Padding(4);
            this.btnConstructionExample.Name = "btnConstructionExample";
            this.btnConstructionExample.Size = new System.Drawing.Size(580, 23);
            this.btnConstructionExample.TabIndex = 12;
            this.btnConstructionExample.Text = "РГ1";
            this.btnConstructionExample.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConstructionExample.UseVisualStyleBackColor = true;
            this.btnConstructionExample.Visible = false;
            // 
            // panelRegularEvent
            // 
            this.panelRegularEvent.AutoSize = true;
            this.panelRegularEvent.BackColor = System.Drawing.Color.LightGray;
            this.panelRegularEvent.Controls.Add(this.btnRegularDKA);
            this.panelRegularEvent.Controls.Add(this.btnRegularNKA);
            this.panelRegularEvent.Controls.Add(this.btnRegularRemove);
            this.panelRegularEvent.Controls.Add(this.btnRegularEdit);
            this.panelRegularEvent.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRegularEvent.Location = new System.Drawing.Point(0, 72);
            this.panelRegularEvent.Margin = new System.Windows.Forms.Padding(0);
            this.panelRegularEvent.MaximumSize = new System.Drawing.Size(204, 150);
            this.panelRegularEvent.Name = "panelRegularEvent";
            this.panelRegularEvent.Padding = new System.Windows.Forms.Padding(4);
            this.panelRegularEvent.Size = new System.Drawing.Size(204, 120);
            this.panelRegularEvent.TabIndex = 0;
            this.panelRegularEvent.Visible = false;
            // 
            // btnRegularDKA
            // 
            this.btnRegularDKA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRegularDKA.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegularDKA.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnRegularDKA.FlatAppearance.BorderSize = 0;
            this.btnRegularDKA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnRegularDKA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnRegularDKA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegularDKA.Location = new System.Drawing.Point(4, 88);
            this.btnRegularDKA.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegularDKA.Name = "btnRegularDKA";
            this.btnRegularDKA.Size = new System.Drawing.Size(196, 28);
            this.btnRegularDKA.TabIndex = 8;
            this.btnRegularDKA.Text = "Построить ДКА";
            this.btnRegularDKA.UseVisualStyleBackColor = true;
            this.btnRegularDKA.Click += new System.EventHandler(this.btnRegularDKA_Click);
            // 
            // btnRegularNKA
            // 
            this.btnRegularNKA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRegularNKA.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegularNKA.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnRegularNKA.FlatAppearance.BorderSize = 0;
            this.btnRegularNKA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnRegularNKA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnRegularNKA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegularNKA.Location = new System.Drawing.Point(4, 60);
            this.btnRegularNKA.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegularNKA.Name = "btnRegularNKA";
            this.btnRegularNKA.Size = new System.Drawing.Size(196, 28);
            this.btnRegularNKA.TabIndex = 7;
            this.btnRegularNKA.Text = "Построить НКА";
            this.btnRegularNKA.UseVisualStyleBackColor = true;
            this.btnRegularNKA.Click += new System.EventHandler(this.btnRegularNKA_Click);
            // 
            // btnRegularRemove
            // 
            this.btnRegularRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRegularRemove.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegularRemove.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnRegularRemove.FlatAppearance.BorderSize = 0;
            this.btnRegularRemove.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnRegularRemove.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnRegularRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegularRemove.Location = new System.Drawing.Point(4, 32);
            this.btnRegularRemove.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegularRemove.Name = "btnRegularRemove";
            this.btnRegularRemove.Size = new System.Drawing.Size(196, 28);
            this.btnRegularRemove.TabIndex = 9;
            this.btnRegularRemove.Text = "Удалить";
            this.btnRegularRemove.UseVisualStyleBackColor = true;
            this.btnRegularRemove.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRegularEdit
            // 
            this.btnRegularEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRegularEdit.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegularEdit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnRegularEdit.FlatAppearance.BorderSize = 0;
            this.btnRegularEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnRegularEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnRegularEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegularEdit.Location = new System.Drawing.Point(4, 4);
            this.btnRegularEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegularEdit.Name = "btnRegularEdit";
            this.btnRegularEdit.Size = new System.Drawing.Size(196, 28);
            this.btnRegularEdit.TabIndex = 3;
            this.btnRegularEdit.Text = "Изменить";
            this.btnRegularEdit.UseVisualStyleBackColor = true;
            this.btnRegularEdit.Click += new System.EventHandler(this.btnRegularEdit_Click);
            // 
            // panelList
            // 
            this.panelList.AutoScroll = true;
            this.panelList.BackColor = System.Drawing.Color.Silver;
            this.panelList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelList.Location = new System.Drawing.Point(0, 360);
            this.panelList.Margin = new System.Windows.Forms.Padding(0);
            this.panelList.Name = "panelList";
            this.panelList.Padding = new System.Windows.Forms.Padding(4);
            this.panelList.Size = new System.Drawing.Size(204, 201);
            this.panelList.TabIndex = 1;
            // 
            // panelRegular
            // 
            this.panelRegular.BackColor = System.Drawing.Color.Silver;
            this.panelRegular.Controls.Add(this.lblGrammatic);
            this.panelRegular.Controls.Add(this.panel1);
            this.panelRegular.Controls.Add(this.panel2);
            this.panelRegular.Controls.Add(this.lblHeaderGenerationChains);
            this.panelRegular.Controls.Add(this.lstChains);
            this.panelRegular.Location = new System.Drawing.Point(0, 302);
            this.panelRegular.Margin = new System.Windows.Forms.Padding(0);
            this.panelRegular.Name = "panelRegular";
            this.panelRegular.Size = new System.Drawing.Size(580, 259);
            this.panelRegular.TabIndex = 1;
            this.panelRegular.Visible = false;
            // 
            // lblGrammatic
            // 
            this.lblGrammatic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGrammatic.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblGrammatic.Location = new System.Drawing.Point(0, 76);
            this.lblGrammatic.Name = "lblGrammatic";
            this.lblGrammatic.Size = new System.Drawing.Size(395, 183);
            this.lblGrammatic.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnStopGenerating);
            this.panel1.Controls.Add(this.lblCountChains);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 48);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(3);
            this.panel1.Size = new System.Drawing.Size(395, 28);
            this.panel1.TabIndex = 2;
            // 
            // btnStopGenerating
            // 
            this.btnStopGenerating.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnStopGenerating.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnStopGenerating.Enabled = false;
            this.btnStopGenerating.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnStopGenerating.FlatAppearance.BorderSize = 0;
            this.btnStopGenerating.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnStopGenerating.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnStopGenerating.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStopGenerating.Location = new System.Drawing.Point(261, 3);
            this.btnStopGenerating.Margin = new System.Windows.Forms.Padding(4);
            this.btnStopGenerating.Name = "btnStopGenerating";
            this.btnStopGenerating.Size = new System.Drawing.Size(134, 22);
            this.btnStopGenerating.TabIndex = 10;
            this.btnStopGenerating.Text = "Остановить";
            this.btnStopGenerating.UseVisualStyleBackColor = true;
            this.btnStopGenerating.Click += new System.EventHandler(this.btnStopGenerating_Click);
            // 
            // lblCountChains
            // 
            this.lblCountChains.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblCountChains.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCountChains.Location = new System.Drawing.Point(3, 3);
            this.lblCountChains.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCountChains.Name = "lblCountChains";
            this.lblCountChains.Size = new System.Drawing.Size(258, 22);
            this.lblCountChains.TabIndex = 3;
            this.lblCountChains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnGenerateChains);
            this.panel2.Controls.Add(this.numMaxLength);
            this.panel2.Controls.Add(this.lblRegLengthMax);
            this.panel2.Controls.Add(this.numMinLength);
            this.panel2.Controls.Add(this.lblRegLengthMin);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 20);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(3);
            this.panel2.Size = new System.Drawing.Size(395, 28);
            this.panel2.TabIndex = 3;
            // 
            // btnGenerateChains
            // 
            this.btnGenerateChains.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnGenerateChains.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnGenerateChains.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnGenerateChains.FlatAppearance.BorderSize = 0;
            this.btnGenerateChains.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnGenerateChains.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnGenerateChains.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateChains.Location = new System.Drawing.Point(261, 3);
            this.btnGenerateChains.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateChains.Name = "btnGenerateChains";
            this.btnGenerateChains.Size = new System.Drawing.Size(134, 22);
            this.btnGenerateChains.TabIndex = 9;
            this.btnGenerateChains.Text = "Сгенерировать";
            this.btnGenerateChains.UseVisualStyleBackColor = true;
            this.btnGenerateChains.Click += new System.EventHandler(this.btnGenerateChains_Click);
            // 
            // numMaxLength
            // 
            this.numMaxLength.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numMaxLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numMaxLength.Dock = System.Windows.Forms.DockStyle.Left;
            this.numMaxLength.Location = new System.Drawing.Point(216, 3);
            this.numMaxLength.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numMaxLength.Name = "numMaxLength";
            this.numMaxLength.Size = new System.Drawing.Size(45, 22);
            this.numMaxLength.TabIndex = 5;
            this.numMaxLength.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // lblRegLengthMax
            // 
            this.lblRegLengthMax.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRegLengthMax.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegLengthMax.Location = new System.Drawing.Point(159, 3);
            this.lblRegLengthMax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegLengthMax.Name = "lblRegLengthMax";
            this.lblRegLengthMax.Size = new System.Drawing.Size(57, 22);
            this.lblRegLengthMax.TabIndex = 3;
            this.lblRegLengthMax.Text = "до";
            this.lblRegLengthMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numMinLength
            // 
            this.numMinLength.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numMinLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numMinLength.Dock = System.Windows.Forms.DockStyle.Left;
            this.numMinLength.Location = new System.Drawing.Point(114, 3);
            this.numMinLength.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numMinLength.Name = "numMinLength";
            this.numMinLength.Size = new System.Drawing.Size(45, 22);
            this.numMinLength.TabIndex = 4;
            // 
            // lblRegLengthMin
            // 
            this.lblRegLengthMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblRegLengthMin.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblRegLengthMin.Location = new System.Drawing.Point(3, 3);
            this.lblRegLengthMin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegLengthMin.Name = "lblRegLengthMin";
            this.lblRegLengthMin.Size = new System.Drawing.Size(111, 22);
            this.lblRegLengthMin.TabIndex = 2;
            this.lblRegLengthMin.Text = "Длина от";
            this.lblRegLengthMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeaderGenerationChains
            // 
            this.lblHeaderGenerationChains.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeaderGenerationChains.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHeaderGenerationChains.Location = new System.Drawing.Point(0, 0);
            this.lblHeaderGenerationChains.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderGenerationChains.Name = "lblHeaderGenerationChains";
            this.lblHeaderGenerationChains.Size = new System.Drawing.Size(395, 20);
            this.lblHeaderGenerationChains.TabIndex = 1;
            this.lblHeaderGenerationChains.Text = "Генерация цепочек";
            this.lblHeaderGenerationChains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstChains
            // 
            this.lstChains.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstChains.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstChains.Dock = System.Windows.Forms.DockStyle.Right;
            this.lstChains.FormattingEnabled = true;
            this.lstChains.ItemHeight = 16;
            this.lstChains.Location = new System.Drawing.Point(395, 0);
            this.lstChains.Name = "lstChains";
            this.lstChains.Size = new System.Drawing.Size(185, 259);
            this.lstChains.TabIndex = 4;
            this.lstChains.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstChains_MouseDoubleClick);
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelHeader.Controls.Add(this.btnCopy);
            this.panelHeader.Controls.Add(this.txtNameConstruction);
            this.panelHeader.Controls.Add(this.lblTypeConstruction);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Margin = new System.Windows.Forms.Padding(0);
            this.panelHeader.MaximumSize = new System.Drawing.Size(204, 72);
            this.panelHeader.MinimumSize = new System.Drawing.Size(204, 72);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Padding = new System.Windows.Forms.Padding(4);
            this.panelHeader.Size = new System.Drawing.Size(204, 72);
            this.panelHeader.TabIndex = 1;
            // 
            // btnCopy
            // 
            this.btnCopy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCopy.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCopy.Enabled = false;
            this.btnCopy.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCopy.FlatAppearance.BorderSize = 0;
            this.btnCopy.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCopy.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCopy.Location = new System.Drawing.Point(4, 46);
            this.btnCopy.Margin = new System.Windows.Forms.Padding(4);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(196, 28);
            this.btnCopy.TabIndex = 2;
            this.btnCopy.Text = "Дублировать";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // txtNameConstruction
            // 
            this.txtNameConstruction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtNameConstruction.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNameConstruction.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtNameConstruction.Location = new System.Drawing.Point(4, 24);
            this.txtNameConstruction.Margin = new System.Windows.Forms.Padding(4);
            this.txtNameConstruction.MaxLength = 22;
            this.txtNameConstruction.Name = "txtNameConstruction";
            this.txtNameConstruction.Size = new System.Drawing.Size(196, 22);
            this.txtNameConstruction.TabIndex = 1;
            this.txtNameConstruction.Visible = false;
            this.txtNameConstruction.TextChanged += new System.EventHandler(this.txtNameConstruction_TextChanged);
            // 
            // lblTypeConstruction
            // 
            this.lblTypeConstruction.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTypeConstruction.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTypeConstruction.Location = new System.Drawing.Point(4, 4);
            this.lblTypeConstruction.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTypeConstruction.Name = "lblTypeConstruction";
            this.lblTypeConstruction.Size = new System.Drawing.Size(196, 20);
            this.lblTypeConstruction.TabIndex = 0;
            this.lblTypeConstruction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelRight
            // 
            this.panelRight.BackColor = System.Drawing.Color.Silver;
            this.panelRight.Controls.Add(this.panelList);
            this.panelRight.Controls.Add(this.lblHeaderConstruction);
            this.panelRight.Controls.Add(this.panelKAEvent);
            this.panelRight.Controls.Add(this.panelRegularEvent);
            this.panelRight.Controls.Add(this.panelHeader);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(580, 24);
            this.panelRight.Margin = new System.Windows.Forms.Padding(0);
            this.panelRight.MinimumSize = new System.Drawing.Size(204, 561);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(204, 561);
            this.panelRight.TabIndex = 1;
            // 
            // lblHeaderConstruction
            // 
            this.lblHeaderConstruction.BackColor = System.Drawing.Color.Silver;
            this.lblHeaderConstruction.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeaderConstruction.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHeaderConstruction.Location = new System.Drawing.Point(0, 340);
            this.lblHeaderConstruction.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderConstruction.Name = "lblHeaderConstruction";
            this.lblHeaderConstruction.Size = new System.Drawing.Size(204, 20);
            this.lblHeaderConstruction.TabIndex = 12;
            this.lblHeaderConstruction.Text = "Конструкции";
            this.lblHeaderConstruction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelKAEvent
            // 
            this.panelKAEvent.AutoSize = true;
            this.panelKAEvent.BackColor = System.Drawing.Color.LightGray;
            this.panelKAEvent.Controls.Add(this.btnBuildRG);
            this.panelKAEvent.Controls.Add(this.btnBuildDKAfromNKA);
            this.panelKAEvent.Controls.Add(this.btnMinimize);
            this.panelKAEvent.Controls.Add(this.btnRemoveKA);
            this.panelKAEvent.Controls.Add(this.btnEditKA);
            this.panelKAEvent.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelKAEvent.Location = new System.Drawing.Point(0, 192);
            this.panelKAEvent.Margin = new System.Windows.Forms.Padding(0);
            this.panelKAEvent.MaximumSize = new System.Drawing.Size(204, 150);
            this.panelKAEvent.Name = "panelKAEvent";
            this.panelKAEvent.Padding = new System.Windows.Forms.Padding(4);
            this.panelKAEvent.Size = new System.Drawing.Size(204, 148);
            this.panelKAEvent.TabIndex = 4;
            this.panelKAEvent.Visible = false;
            // 
            // btnBuildRG
            // 
            this.btnBuildRG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnBuildRG.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBuildRG.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnBuildRG.FlatAppearance.BorderSize = 0;
            this.btnBuildRG.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnBuildRG.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnBuildRG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuildRG.Location = new System.Drawing.Point(4, 116);
            this.btnBuildRG.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuildRG.Name = "btnBuildRG";
            this.btnBuildRG.Size = new System.Drawing.Size(196, 28);
            this.btnBuildRG.TabIndex = 7;
            this.btnBuildRG.Text = "Построить РГ";
            this.btnBuildRG.UseVisualStyleBackColor = true;
            this.btnBuildRG.Click += new System.EventHandler(this.btnBuildRG_Click);
            // 
            // btnBuildDKAfromNKA
            // 
            this.btnBuildDKAfromNKA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnBuildDKAfromNKA.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBuildDKAfromNKA.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnBuildDKAfromNKA.FlatAppearance.BorderSize = 0;
            this.btnBuildDKAfromNKA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnBuildDKAfromNKA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnBuildDKAfromNKA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuildDKAfromNKA.Location = new System.Drawing.Point(4, 88);
            this.btnBuildDKAfromNKA.Margin = new System.Windows.Forms.Padding(4);
            this.btnBuildDKAfromNKA.Name = "btnBuildDKAfromNKA";
            this.btnBuildDKAfromNKA.Size = new System.Drawing.Size(196, 28);
            this.btnBuildDKAfromNKA.TabIndex = 8;
            this.btnBuildDKAfromNKA.Text = "Построить ДКА";
            this.btnBuildDKAfromNKA.UseVisualStyleBackColor = true;
            this.btnBuildDKAfromNKA.Click += new System.EventHandler(this.btnBuildDKAfromNKA_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMinimize.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMinimize.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(4, 60);
            this.btnMinimize.Margin = new System.Windows.Forms.Padding(4);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(196, 28);
            this.btnMinimize.TabIndex = 10;
            this.btnMinimize.Text = "Минимизировать";
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnRemoveKA
            // 
            this.btnRemoveKA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnRemoveKA.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRemoveKA.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnRemoveKA.FlatAppearance.BorderSize = 0;
            this.btnRemoveKA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnRemoveKA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnRemoveKA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveKA.Location = new System.Drawing.Point(4, 32);
            this.btnRemoveKA.Margin = new System.Windows.Forms.Padding(4);
            this.btnRemoveKA.Name = "btnRemoveKA";
            this.btnRemoveKA.Size = new System.Drawing.Size(196, 28);
            this.btnRemoveKA.TabIndex = 9;
            this.btnRemoveKA.Text = "Удалить";
            this.btnRemoveKA.UseVisualStyleBackColor = true;
            this.btnRemoveKA.Click += new System.EventHandler(this.btnRemoveKA_Click);
            // 
            // btnEditKA
            // 
            this.btnEditKA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnEditKA.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditKA.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEditKA.FlatAppearance.BorderSize = 0;
            this.btnEditKA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnEditKA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnEditKA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditKA.Location = new System.Drawing.Point(4, 4);
            this.btnEditKA.Margin = new System.Windows.Forms.Padding(4);
            this.btnEditKA.Name = "btnEditKA";
            this.btnEditKA.Size = new System.Drawing.Size(196, 28);
            this.btnEditKA.TabIndex = 3;
            this.btnEditKA.Text = "Изменить";
            this.btnEditKA.UseVisualStyleBackColor = true;
            this.btnEditKA.Click += new System.EventHandler(this.btnEditKA_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.добавитьToolStripMenuItem,
            this.видToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.toolStripMenuItem1,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(169, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.регулярнуюГрамматикуToolStripMenuItem,
            this.конечныйАвтоматToolStripMenuItem});
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.добавитьToolStripMenuItem.Text = "Добавить";
            // 
            // регулярнуюГрамматикуToolStripMenuItem
            // 
            this.регулярнуюГрамматикуToolStripMenuItem.Name = "регулярнуюГрамматикуToolStripMenuItem";
            this.регулярнуюГрамматикуToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.регулярнуюГрамматикуToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.регулярнуюГрамматикуToolStripMenuItem.Text = "Регулярную грамматику";
            this.регулярнуюГрамматикуToolStripMenuItem.Click += new System.EventHandler(this.регулярнуюГрамматикуToolStripMenuItem_Click);
            // 
            // конечныйАвтоматToolStripMenuItem
            // 
            this.конечныйАвтоматToolStripMenuItem.Name = "конечныйАвтоматToolStripMenuItem";
            this.конечныйАвтоматToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this.конечныйАвтоматToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.конечныйАвтоматToolStripMenuItem.Text = "Конечный автомат";
            this.конечныйАвтоматToolStripMenuItem.Click += new System.EventHandler(this.конечныйАвтоматToolStripMenuItem_Click);
            // 
            // видToolStripMenuItem
            // 
            this.видToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отображатьГрафКАToolStripMenuItem});
            this.видToolStripMenuItem.Name = "видToolStripMenuItem";
            this.видToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.видToolStripMenuItem.Text = "Вид";
            // 
            // отображатьГрафКАToolStripMenuItem
            // 
            this.отображатьГрафКАToolStripMenuItem.Name = "отображатьГрафКАToolStripMenuItem";
            this.отображатьГрафКАToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.отображатьГрафКАToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.отображатьГрафКАToolStripMenuItem.Text = "Граф КА";
            this.отображатьГрафКАToolStripMenuItem.Click += new System.EventHandler(this.отображатьГрафКАToolStripMenuItem_Click);
            // 
            // panelKA
            // 
            this.panelKA.BackColor = System.Drawing.Color.Silver;
            this.panelKA.Controls.Add(this.panel3);
            this.panelKA.Controls.Add(this.lblKAHeader);
            this.panelKA.Location = new System.Drawing.Point(3, 27);
            this.panelKA.Name = "panelKA";
            this.panelKA.Size = new System.Drawing.Size(577, 268);
            this.panelKA.TabIndex = 3;
            this.panelKA.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtKALog);
            this.panel3.Controls.Add(this.lblKA);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 20);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(577, 248);
            this.panel3.TabIndex = 12;
            // 
            // txtKALog
            // 
            this.txtKALog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtKALog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKALog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKALog.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKALog.Location = new System.Drawing.Point(0, 54);
            this.txtKALog.Margin = new System.Windows.Forms.Padding(4);
            this.txtKALog.MaxLength = 32768;
            this.txtKALog.Multiline = true;
            this.txtKALog.Name = "txtKALog";
            this.txtKALog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtKALog.Size = new System.Drawing.Size(577, 194);
            this.txtKALog.TabIndex = 14;
            // 
            // lblKA
            // 
            this.lblKA.AutoSize = true;
            this.lblKA.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblKA.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblKA.Location = new System.Drawing.Point(0, 31);
            this.lblKA.Name = "lblKA";
            this.lblKA.Size = new System.Drawing.Size(0, 23);
            this.lblKA.TabIndex = 15;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnCheck);
            this.panel4.Controls.Add(this.txtChain);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(3);
            this.panel4.Size = new System.Drawing.Size(577, 31);
            this.panel4.TabIndex = 13;
            // 
            // btnCheck
            // 
            this.btnCheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCheck.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnCheck.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCheck.FlatAppearance.BorderSize = 0;
            this.btnCheck.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnCheck.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightGray;
            this.btnCheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheck.Location = new System.Drawing.Point(426, 3);
            this.btnCheck.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(155, 25);
            this.btnCheck.TabIndex = 10;
            this.btnCheck.Text = "Распознать";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // txtChain
            // 
            this.txtChain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtChain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtChain.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtChain.Location = new System.Drawing.Point(3, 3);
            this.txtChain.Margin = new System.Windows.Forms.Padding(4);
            this.txtChain.MaxLength = 22;
            this.txtChain.Name = "txtChain";
            this.txtChain.Size = new System.Drawing.Size(423, 22);
            this.txtChain.TabIndex = 11;
            // 
            // lblKAHeader
            // 
            this.lblKAHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblKAHeader.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblKAHeader.Location = new System.Drawing.Point(0, 0);
            this.lblKAHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKAHeader.Name = "lblKAHeader";
            this.lblKAHeader.Size = new System.Drawing.Size(577, 20);
            this.lblKAHeader.TabIndex = 2;
            this.lblKAHeader.Text = "Распознавание цепочек";
            this.lblKAHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btnConstructionExample);
            this.Controls.Add(this.panelKA);
            this.Controls.Add(this.panelRegular);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "Main";
            this.ShowIcon = false;
            this.Text = "Генератор и разпознаватель регулярных языков";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.panelRegularEvent.ResumeLayout(false);
            this.panelRegular.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numMaxLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinLength)).EndInit();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelKAEvent.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelKA.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelRegularEvent;
        private System.Windows.Forms.Panel panelList;
        private System.Windows.Forms.Panel panelRegular;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.TextBox txtNameConstruction;
        private System.Windows.Forms.Label lblTypeConstruction;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Button btnRegularDKA;
        private System.Windows.Forms.Button btnRegularNKA;
        private System.Windows.Forms.Button btnRegularEdit;
        private System.Windows.Forms.Label lblHeaderConstruction;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnGenerateChains;
        private System.Windows.Forms.NumericUpDown numMaxLength;
        private System.Windows.Forms.Label lblRegLengthMax;
        private System.Windows.Forms.NumericUpDown numMinLength;
        private System.Windows.Forms.Label lblRegLengthMin;
        private System.Windows.Forms.Label lblHeaderGenerationChains;
        private System.Windows.Forms.ListBox lstChains;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.Button btnStopGenerating;
        private System.Windows.Forms.Button btnRegularRemove;
        private System.Windows.Forms.Label lblCountChains;
        private System.Windows.Forms.Label lblGrammatic;
        private System.Windows.Forms.ToolStripMenuItem регулярнуюГрамматикуToolStripMenuItem;
        private System.Windows.Forms.Button btnConstructionExample;
        private System.Windows.Forms.ToolStripMenuItem конечныйАвтоматToolStripMenuItem;
        private System.Windows.Forms.Panel panelKA;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtChain;
        private System.Windows.Forms.Label lblKAHeader;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtKALog;
        private System.Windows.Forms.Panel panelKAEvent;
        private System.Windows.Forms.Button btnBuildDKAfromNKA;
        private System.Windows.Forms.Button btnBuildRG;
        private System.Windows.Forms.Button btnRemoveKA;
        private System.Windows.Forms.Button btnEditKA;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.ToolStripMenuItem видToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отображатьГрафКАToolStripMenuItem;
        private System.Windows.Forms.Label lblKA;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
    }
}

