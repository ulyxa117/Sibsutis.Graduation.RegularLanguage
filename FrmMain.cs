﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegLangDiplom
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        public class genChainsStruct
        {
            public int minLength;
            public int maxLength;
            public ListBox lstOut;
            public Button btnStart;
            public Button btnStop;
            public Label lblCount;
            public List<generatingChains> chains;
        }

        public class generatingChains
        {
            public generatingChains(string chain, string path)
            {
                this.chain = chain;
                this.path = path;
            }
            public string chain;
            public string path;
        }

        private List<TConstruction> constructs = new List<TConstruction>();
        private TConstruction currentConstructs;
        private Button currentButton;
        private frmKAPainter KAPainter = null;
        private Thread threadGenerateChains;
        public List<generatingChains> chains;

        private void регулярнуюГрамматикуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEditGrammatic dlg = new FrmEditGrammatic();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                addConstruction(dlg.gram);
            }
            dlg.Dispose();
        }

        private void ChangeCurrentConstruction(object sender, EventArgs e)
        {
            txtNameConstruction.Visible = true;
            btnCopy.Enabled = true;
            currentButton = (Button)sender;
            currentConstructs = (TConstruction)currentButton.Tag;
            lblTypeConstruction.Text = currentConstructs.getTypeString();
            if (currentConstructs.getTypeString() == "РГ")
            {
                panelRegular.Visible = panelRegularEvent.Visible = true;
                panelKA.Visible = panelKAEvent.Visible = false;
                lblGrammatic.Text = currentConstructs.getString();
            } else
            {
                panelRegular.Visible = panelRegularEvent.Visible = false;
                panelKA.Visible = panelKAEvent.Visible = true;
                if (currentConstructs.getTypeString() == "ДКА")
                {
                    btnMinimize.Visible = true;
                    btnBuildRG.Visible = true;
                    btnBuildDKAfromNKA.Visible = false;
                } else
                {
                    btnMinimize.Visible = false;
                    btnBuildRG.Visible = false;
                    btnBuildDKAfromNKA.Visible = true;
                }
                lblKA.Text = currentConstructs.getString();
                paintKA();
            }
            lblTypeConstruction.Text = currentConstructs.getTypeString();
            txtNameConstruction.Text = currentConstructs.getName();
        }

        private void addConstruction(TConstruction c)
        {
            constructs.Add(c);
            Button btn = new Button();
            btn.Font = btnConstructionExample.Font;
            btn.FlatStyle = btnConstructionExample.FlatStyle;
            btn.FlatAppearance.BorderColor = btnConstructionExample.FlatAppearance.BorderColor;
            btn.FlatAppearance.BorderSize = btnConstructionExample.FlatAppearance.BorderSize;
            btn.FlatAppearance.MouseDownBackColor = btnConstructionExample.FlatAppearance.MouseDownBackColor;
            btn.FlatAppearance.MouseOverBackColor = btnConstructionExample.FlatAppearance.MouseOverBackColor;
            btn.Text = c.getName();
            btn.Tag = c;
            btn.Dock = DockStyle.Top;
            btn.Click += new EventHandler(ChangeCurrentConstruction);
            btn.Parent = panelList;
            btn.PerformClick();
        }

        private void txtNameConstruction_TextChanged(object sender, EventArgs e)
        {
            currentConstructs.setName(txtNameConstruction.Text);
            currentButton.Text = txtNameConstruction.Text;
        }

        private void btnRegularEdit_Click(object sender, EventArgs e)
        {
            FrmEditGrammatic dlg = new FrmEditGrammatic();
            dlg.setData((TGrammatic)currentConstructs);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentConstructs = dlg.gram.Copy();
                currentConstructs.setName(((TGrammatic)currentButton.Tag).getName());
                currentButton.Tag = currentConstructs;
                lblGrammatic.Text = currentConstructs.getString();
            }
            dlg.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите удалить эту грамматику из проекта?", "Удалить грамматику",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                constructs.Remove(currentConstructs);
                currentButton.Dispose();
                panelRegular.Visible = panelRegularEvent.Visible = false;
                btnCopy.Enabled = false;
                txtNameConstruction.Text = "";
                lblTypeConstruction.Text = "";
            };
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            addConstruction(currentConstructs.Copy());
            txtNameConstruction.Text = "Копия " + txtNameConstruction.Text;
        }

        private void конечныйАвтоматToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEditKA dlg = new FrmEditKA();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                addConstruction(dlg.KA);
            }
            dlg.Dispose();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            panelRegular.Dock = DockStyle.Fill;
            panelKA.Dock = DockStyle.Fill;
            string[] s = Environment.GetCommandLineArgs();
            if (s.Length > 1)
            {
                LoadFromFile(s[1]);
            }
        }

        private void btnRemoveKA_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите удалить этот автомат из проекта?", "Удалить автомат",
               MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                constructs.Remove(currentConstructs);
                currentButton.Dispose();
                panelKA.Visible = panelKAEvent.Visible = false;
                btnCopy.Enabled = false;
                txtNameConstruction.Text = "";
                lblTypeConstruction.Text = "";
            };
        }

        private void btnEditKA_Click(object sender, EventArgs e)
        {
            FrmEditKA dlg = new FrmEditKA();
            dlg.setData((TKA)currentConstructs);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                currentConstructs = dlg.KA;
                currentConstructs.setName(((TKA)currentButton.Tag).getName());
                currentButton.Tag = currentConstructs;
                lblTypeConstruction.Text = currentConstructs.getTypeString();
                lblKA.Text = currentConstructs.getString();
            }
            dlg.Dispose();
        }

        private void PainterClose(Object sender, FormClosingEventArgs e)
        {
            отображатьГрафКАToolStripMenuItem.Checked = false;
            if (KAPainter != null)
            {
                KAPainter.Dispose();
                KAPainter = null;
            }
        }

        private void paintKA()
        {
            if (отображатьГрафКАToolStripMenuItem.Checked && currentConstructs != null && currentConstructs.getTypeString() != "РГ")
            {
                if (KAPainter == null)
                {
                    KAPainter = new frmKAPainter();
                    KAPainter.Show();
                    KAPainter.FormClosing += PainterClose;
                }
                KAPainter.setData(((TKA)currentConstructs).Painter);
            }
        }

        private void отображатьГрафКАToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (отображатьГрафКАToolStripMenuItem.Checked)
            {
                отображатьГрафКАToolStripMenuItem.Checked = false;
                if (KAPainter != null)
                {
                    KAPainter.Dispose();
                    KAPainter = null;
                }
            } else
            {
                отображатьГрафКАToolStripMenuItem.Checked = true;
                paintKA();
            }
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            txtKALog.Text += Environment.NewLine + ((TKA)currentConstructs).CheckLine(txtChain.Text);
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            currentConstructs = ((TDKA)currentConstructs).Minimization();
            currentButton.Tag = currentConstructs;
            txtNameConstruction.Text = currentButton.Text = currentConstructs.getName();
            lblKA.Text = currentConstructs.getString();
        }

        private void btnBuildDKAfromNKA_Click(object sender, EventArgs e)
        {
            addConstruction(((TNKA)currentConstructs).ConvertToDKA());
        }

        private void btnBuildRG_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Нажмите 'Да', для получения ЛЛ грамматики, или 'Нет' - для ПЛ", "Построение грамматики по ДКА", MessageBoxButtons.YesNoCancel);
            if (result == DialogResult.Yes)
                addConstruction(((TDKA)currentConstructs).ConvertToLLGram());
            else
                if (result == DialogResult.No)
                addConstruction(((TDKA)currentConstructs).ConvertToPLGram());
        }

        private void btnRegularNKA_Click(object sender, EventArgs e)
        {
            addConstruction(((TGrammatic)currentConstructs).ConvertToNKA());
        }

        private void btnRegularDKA_Click(object sender, EventArgs e)
        {
            addConstruction(((TGrammatic)currentConstructs).ConvertToDKA());
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "RegLang files (*.rlf)|*.rlf";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                StreamWriter f = new StreamWriter(dlg.FileName);
                foreach (TConstruction c in constructs)
                    c.SaveToFile(f);
                f.Close();
                MessageBox.Show("Проект успешно сохранен", "Сохранение проекта", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Text = "Генератор и разпознаватель регулярных языков - " + dlg.FileName.Remove(0 , dlg.FileName.LastIndexOf('\\') + 1);
            }
        }

        private void LoadFromFile(string filename)
        {
            try
            {
                for (int i = 0; i < panelList.Controls.Count;)
                    panelList.Controls[i].Dispose();
                constructs.Clear();
                panelRegular.Visible = panelRegularEvent.Visible = false;
                btnCopy.Enabled = false;
                txtNameConstruction.Text = "";
                lblTypeConstruction.Text = "";

                StreamReader f = new StreamReader(filename);
                while (!f.EndOfStream)
                {
                    string s = f.ReadLine();
                    TConstruction newConst;
                    if (s == "[RG-Start]")
                    {
                        newConst = new TGrammatic();
                    }
                    else
                    {
                        s = f.ReadLine();
                        if (s.EndsWith("DKA")) newConst = new TDKA(0, "");
                        else
                            newConst = new TNKA(0, "");
                    }
                    newConst.LoadFromFile(f);
                    addConstruction(newConst);
                }
                f.Close();
                Text = "Генератор и разпознаватель регулярных языков - " + filename.Remove(0, filename.LastIndexOf('\\') + 1);
            }
            catch
            {
                MessageBox.Show("Не удалось открыть проект", "Открытие проекта", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "RegLang files (*.rlf)|*.rlf";
            if (dlg.ShowDialog() == DialogResult.OK)
                LoadFromFile(dlg.FileName);
        }

        public void endGenerating()
        {
            btnGenerateChains.Enabled = true;
            btnStopGenerating.Enabled = false;
        }

        private void btnGenerateChains_Click(object sender, EventArgs e)
        {
            if (numMinLength.Value <= numMaxLength.Value)
            {
                threadGenerateChains = new Thread(((TGrammatic)currentConstructs).GenerateChains);
                genChainsStruct p = new genChainsStruct();
                p.minLength = Convert.ToInt32(numMinLength.Value);
                p.maxLength = Convert.ToInt32(numMaxLength.Value);
                p.lstOut = lstChains;
                p.btnStart = btnGenerateChains;
                p.btnStop = btnStopGenerating;
                p.lblCount = lblCountChains;
                p.chains = chains = new List<generatingChains>();
                btnGenerateChains.Enabled = false;
                btnStopGenerating.Enabled = true;
                lblCountChains.Text = "0";
                lstChains.Items.Clear();
                threadGenerateChains.Start(p);
            }
        }

        private void btnStopGenerating_Click(object sender, EventArgs e)
        {
            if (threadGenerateChains.IsAlive) threadGenerateChains.Abort();
            endGenerating();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (threadGenerateChains != null && threadGenerateChains.IsAlive) threadGenerateChains.Abort();
        }

        private void lstChains_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lstChains.SelectedIndex >= 0)
                MessageBox.Show(chains.Find(x => x.chain == lstChains.SelectedItem.ToString()).path);
        }
    }
}