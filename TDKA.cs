﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegLangDiplom
{
    public class TDKA : TKA
    {
        public TDKA(int countStates, string alphabet) : base(countStates, alphabet)
        {
            type = TType.DKA;
        }

        public override string getTypeString()
        {
            return "ДКА";
        }

        public override string CheckLine(string chain)
        {
            int q = StartState;
            string outstr = getKonf(q, chain);
            while (chain.Length > 0)
            {
                int index = Alphabet.IndexOf(chain[0]);
                if (index < 0 || Table[q, Alphabet.IndexOf(chain[0])].Count == 0) break;
                q = Table[q, index].First();
                chain = chain.Remove(0, 1);
                outstr += "├" + getKonf(q, chain);
            }
            if (chain.Length > 0)
                outstr += "├STOP(-) Нет подходящего правила перехода";
            else
                if (!FinishStates.Contains(q))
                outstr += "├STOP(-) Автомат не пришел в конечное состояние";
            else outstr += "├STOP(+)";
            return outstr + Environment.NewLine;
        }

        private void findStates(int state, ref List<int> outList)
        {
            outList.Add(state);
            for (int i = 0; i < Alphabet.Length; i++)
                foreach (int k in Table[state, i])
                    if (!outList.Contains(k)) findStates(k, ref outList);
        }

        public TDKA Minimization()
        {
            List<int> states = new List<int>();
            findStates(StartState, ref states);
            List<List<int>> R = new List<List<int>>();
            List<int> F = new List<int>(FinishStates);
            List<int> Q = new List<int>(states.Except(F));
            R.Add(Q);
            R.Add(F);

            int count = R.Count;
            do
            {
                count = R.Count;
                List<List<int>> Rnew = new List<List<int>>();
                foreach (List<int> l in R)
                {
                    int[,] mat = new int[states.Count, Alphabet.Length];
                    for (int i = 0; i < l.Count; i++)
                        for (int j = 0; j < Alphabet.Length; j++)
                            if (Table[l[i], j].Count == 0) mat[l[i], j] = -1;
                            else
                                mat[l[i], j] = R.IndexOf(R.Find(x => x.Contains(Table[l[i], j].First())));
                    List<int> temp = new List<int>(l);
                    while (temp.Count > 0)
                    {
                        List<int> newr = new List<int>();
                        int f = temp.First();
                        newr.Add(f);
                        temp.RemoveAt(0);
                        for (int i = 0; i < temp.Count(); i++)
                        {
                            bool flag = true;
                            for (int j = 0; j < Alphabet.Length; j++)
                                if (mat[temp[i], j] != mat[f, j])
                                {
                                    flag = false;
                                    break;
                                }
                            if (flag)
                            {
                                newr.Add(temp[i]);
                                temp.RemoveAt(i);
                                i--;
                            }
                        }
                        Rnew.Add(newr);
                    }
                }
                R = Rnew;
            } while (count != R.Count);

            TDKA DKA = new TDKA(R.Count, Alphabet);
            DKA.setName("Мини " + Name);
            for (int i = 0; i < DKA.getCountStates(); i++)
                if (R[i].Contains(StartState))
                {
                    DKA.setStartState(i);
                    break;
                }
            F = new List<int>();
            for (int i = 0; i < DKA.getCountStates(); i++)
                if (R[i].Intersect(FinishStates).Count() > 0) F.Add(i);
            DKA.setFinishStates(F);
            int q0 = 0;
            foreach (List<int> l in R)
            {
                int q = l.First();
                for (int i = 0; i < Alphabet.Length; i++)
                    if (Table[q, i].Count > 0) {
                        int index = R.IndexOf(R.Find(x => x.Contains(Table[q, i].First())));
                        DKA.addRule(q0, index,Alphabet[i]);
                    }
                q0++;
            }
            return DKA;
        }

        public TGrammatic ConvertToLLGram()
        {
            string noterms = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            TGrammatic gram = new TGrammatic();
            gram.setName("РГ из " + Name);
            gram.setTerms(Alphabet);
            for (int i = 0; i < CountStates; i++)
            {
                for (int j = 0; j < Alphabet.Length; j++)
                    foreach (int k in Table[i, j])
                        gram.addRule(noterms[k], noterms[i].ToString() + Alphabet[j]);
                if (i == StartState) gram.addRule(noterms[i], "λ");
            }
            int countNoterms = CountStates + (FinishStates.Count == 1 ? 0 : 1);
            gram.setNoterms(noterms.Substring(0, countNoterms));
            if (FinishStates.Count == 1)
                gram.setStart(noterms[FinishStates.First()]);
            else
            {
                foreach (int i in FinishStates)
                    if (gram.getRules().Keys.Contains(noterms[i]))
                        foreach (string s in gram.getRules()[noterms[i]])
                            gram.addRule(noterms[countNoterms - 1], s);
                gram.setStart(noterms[countNoterms - 1]);
            }
            gram.setType(TGrammatic.TType.LL);
            while (gram.removeAloneRules() || gram.removeUnuseNoterms()) { }
            return gram;
        }

        public TGrammatic ConvertToPLGram()
        {
            string noterms = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            TGrammatic gram = new TGrammatic();
            gram.setName("РГ из " + Name);
            gram.setTerms(Alphabet);
            gram.setStart(noterms[StartState]);
            gram.setNoterms(noterms.Substring(0, CountStates));
            for (int i = 0; i < CountStates; i++)
            {
                for (int j = 0; j < Alphabet.Length; j++)
                    foreach(int k in Table[i,j])
                        gram.addRule(noterms[i], Alphabet[j] + noterms[k].ToString());
                if (FinishStates.Contains(i)) gram.addRule(noterms[i], "λ");
            }
            gram.setType(TGrammatic.TType.PL);
            while (gram.removeAloneRules() || gram.removeUnuseNoterms()) { }
            return gram;
        }
    }
}
