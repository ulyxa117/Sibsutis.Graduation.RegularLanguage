﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegLangDiplom
{
    public partial class FrmEditKA : Form
    {
        public FrmEditKA()
        {
            InitializeComponent();
        }

        private class Rule
        {
            public int q0;
            public char c;
            public int q1;
            public Rule(int q0, int q1, char c)
            {
                this.q0 = q0;
                this.q1 = q1;
                this.c = c;
            }
        }

        private List<Rule> rules;
        public TConstruction KA;
        bool edit = false;

        public void setData(TKA KA)
        {
            numCountStates.Value = KA.getCountStates();
            numStartState.Value = KA.getStartState();
            string str = "";
            foreach (int i in KA.getFinishStates()) str += i.ToString() + " ";
            str = str.Remove(str.Length - 1, 1);
            txtFinishStates.Text = str;
            txtAlphabet.Text = KA.getAlphabet();
            rules = new List<Rule>();
            if (KA.getTypeKA() == TKA.TType.DKA)
                rbDKA.Checked = true;
            else
                rbNKA.Checked = true;
            List<int>[,] t = KA.getTable();
            for (int i = 0; i < numCountStates.Value; i++)
                for (int j = 0; j < txtAlphabet.Text.Length; j++)
                    foreach (int k in t[i, j]) 
                        rules.Add(new Rule(i, k, txtAlphabet.Text[j]));
            foreach (Rule r in rules)
                lstRules.Items.Add("{q" + r.q0.ToString() + ", " + r.c + ", q" + r.q1.ToString() + "}");
            edit = true;
        }

        private void numCountStates_ValueChanged(object sender, EventArgs e)
        {
            cbq0.Items.Clear();
            cbq1.Items.Clear();
            for (int i = 0; i < numCountStates.Value; i++)
            {
                cbq0.Items.Add(i);
                cbq1.Items.Add(i);
            }
            numStartState.Maximum = numCountStates.Value - 1;
        }

        private void FrmEditKA_Load(object sender, EventArgs e)
        {
            if (!edit)
            {
                cbq0.Items.Clear();
                cbq0.Items.Add(0);
                cbq1.Items.Clear();
                cbq1.Items.Add(0);
                numStartState.Maximum = numCountStates.Value - 1;
                rules = new List<Rule>();
            }
        }

        private void txtAlphabet_TextChanged(object sender, EventArgs e)
        {
            txtAlphabet.Text = new string(txtAlphabet.Text.Distinct().ToArray());
            string s = cbc.Text.ToString();
            cbc.Items.Clear();
            foreach (char c in txtAlphabet.Text)
                cbc.Items.Add(c);
            if (cbc.Items.Contains(s)) cbc.Text = s;
        }

        private int checkRule(int q0, int q1, char c)
        {
            if (!cbq0.Items.Contains(q0)) return 2;
            if (!cbc.Items.Contains(c)) return 4;
            if (!cbq0.Items.Contains(q1)) return 6;
            return 0;
        }

        private string getErrorString(int code)
        {
            switch (code)
            {
                case 1: return "Задайте в поле 'q0' числовое значение состояния";
                case 2: return "В поле 'q0' указано несуществующее состояние (нумерация состояний с нуля)";
                case 3: return "Не задан символ, по которому осуществляется переход";
                case 4: return "Символ не найден в алфавите";
                case 5: return "Задайте в поле 'q1' числовое значение состояния";
                case 6: return "В поле 'q1' указано несуществующее состояние (нумерация состояний с нуля)";
                case 7: return "Такое правило уже существует";
                default: return "";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int q0 = 0, q1 = 0, code;
            if (!int.TryParse(cbq0.Text, out q0)) code = 1;
            else
                if (cbc.Text == null || cbc.Text.Length < 1) code = 3;
            else
                if (!int.TryParse(cbq1.Text, out q1)) code = 5; else
                code = checkRule(q0, q1, cbc.Text[0]);
            if (code == 0)
                if (rules.Exists(x => x.q0 == q0 && x.q1 == q1 && x.c == cbc.Text[0])) code = 7;
            if (code > 0)
            {
                MessageBox.Show(getErrorString(code), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            rules.Add(new Rule(q0, q1, cbc.Text[0]));
            lstRules.Items.Add("{q" + q0.ToString() + ", " + cbc.Text + ", q" + q1.ToString() + "}");
        }

        private void lstRules_KeyDown(object sender, KeyEventArgs e)
        {
            if (lstRules.SelectedIndex >= 0 && e.KeyCode == Keys.Delete)
            {
                rules.RemoveAll(x => "{q" + x.q0.ToString() + ", " + x.c.ToString() + ", q" + x.q1.ToString() + "}" == lstRules.SelectedItem.ToString());
                lstRules.Items.RemoveAt(lstRules.SelectedIndex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            foreach (Rule r in rules)
            {
                if (rbDKA.Checked && rules.Exists(x => x != r && x.q0 == r.q0 && x.c == r.c))
                {
                    MessageBox.Show("ДКА не может иметь пару правил с одинаковыми 'q0' и 'c'", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                int code = checkRule(r.q0, r.q1, r.c);
                if (code > 0)
                {
                    MessageBox.Show(getErrorString(code), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    lstRules.SelectedIndex = lstRules.FindStringExact("{q" + r.q0.ToString() + ", " + r.c.ToString() + ", q" + r.q1.ToString() + "}");
                    return;
                }
            }
            string temp = "";
            for (int i = 0; i < txtFinishStates.Text.Length; i++)
                if (txtFinishStates.Text[i] >= '0' && txtFinishStates.Text[i] <= '9') temp += txtFinishStates.Text[i];
                else
                    if (temp.Length > 0 && temp[temp.Length - 1] != ' ') temp += ' ';
            temp = temp.TrimEnd(' ');
            if (temp.Length == 0)
            {
                MessageBox.Show("Укажите конечные состояния", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            List<string> fs = temp.Split(' ').Distinct().ToList();
            List<int> finishStates = new List<int>();
            foreach (string s in fs)
            {
                int state = int.Parse(s);
                if (state < 0 || state >= numCountStates.Value)
                {
                    MessageBox.Show("Несуществующее конечное состояние", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                finishStates.Add(state);
            }

            TKA KA;
            if (rbDKA.Checked)
            {
                KA = new TDKA(Convert.ToInt32(numCountStates.Value), txtAlphabet.Text);
                KA.setName("Новый ДКА");
            }
            else
            {
                KA = new TNKA(Convert.ToInt32(numCountStates.Value), txtAlphabet.Text);
                KA.setName("Новый НКА");
            }
            KA.setFinishStates(finishStates);
            KA.setStartState(Convert.ToInt32(numStartState.Value));
            foreach (Rule r in rules)
                KA.addRule(r.q0, r.q1, r.c);
            this.KA = KA;
            DialogResult = DialogResult.OK;
        }
    }
}
