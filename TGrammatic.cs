﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RegLangDiplom
{
    using System.IO;
    using System.Windows.Forms;
    using TRules = Dictionary<char, List<string>>;

    public class TGrammatic : TConstruction
    {
        public enum TType {LL, PL};
        private TRules rules;
        private string Noterms;
        private string Terms;
        private char Start;
        private TType type;

        public TGrammatic()
        {
            Name = "Новая РГ";
            Type = "РГ";
            rules = new TRules();
            Noterms = "";
            Terms = "";
            Start = new char();
        }

        public void setType(TType type)
        {
            this.type = type;
        }

        public void setNoterms(string noterms)
        {
            Noterms = noterms;
        }

        public void setTerms(string terms)
        {
            Terms = terms;
        }

        public void setStart(char symbol)
        {
            Start = symbol;
        }

        public void addRule(char symbol, string line)
        {
            List<string> lst;
            if (rules.Keys.Contains(symbol))
                lst = rules[symbol];
            else
            {
                lst = new List<string>();
                rules.Add(symbol, lst);
            }
            if (!lst.Contains(line)) lst.Add(line);
        }

        public override TConstruction Copy()
        {
            TGrammatic newGram = new TGrammatic();
            newGram.setName(Name);
            newGram.setNoterms(Noterms);
            newGram.setTerms(Terms);
            newGram.setStart(Start);
            newGram.rules = new TRules(rules);
            newGram.setType(type);
            return newGram;
        }

        public override string getTypeString()
        {
            return Type;
        }

        public string getNoterms()
        {
            return Noterms;
        }

        public string getTerms()
        {
            return Terms;
        }

        public char getStart()
        {
            return Start;
        }

        public TRules getRules()
        {
            return rules;
        }

        public void clearRules()
        {
            rules.Clear();
        }

        public override string getString()
        {
            string str = "";
            TRules.Enumerator e = rules.GetEnumerator();
            while (e.MoveNext())
            {
                if (e.Current.Key == Start) str += "*";
                str += e.Current.Key + "→";
                foreach (string s in e.Current.Value) str += s + "|";
                str = str.Remove(str.Length - 1, 1);
                str += Environment.NewLine;
            }
            return str.Length > 0 ? str.Remove(str.Length - 1, 1) : str;
        }

        public bool removeUnuseNoterms()
        {
            TRules.Enumerator e = rules.GetEnumerator();
            List<char> keys = rules.Keys.ToList();
            keys.Remove(Start);
            while (e.MoveNext())
                foreach (string s in e.Current.Value)
                    keys.RemoveAll(x => s.Contains(x.ToString()));
            foreach (char c in keys)
            {
                rules.Remove(c);
                Noterms = Noterms.Remove(Noterms.IndexOf(c), 1);
            }
            return keys.Count > 0;
        }

        public bool removeAloneRules()
        {
            bool flag = false;
            char c = new char();
            string s = "1";
            while (s != "")
            {
                s = "";
                TRules.Enumerator e = rules.GetEnumerator();
                while (e.MoveNext())
                {
                    if (Start != e.Current.Key && e.Current.Value.Count == 1)
                    {
                        flag = true;
                        c = e.Current.Key;
                        s = e.Current.Value.First();
                        break;
                    }
                }
                if (s != "")
                {
                    e = rules.GetEnumerator();
                    while (e.MoveNext()) 
                        while (e.Current.Value.Exists(x => x.IndexOf(c) >= 0))
                        {
                            string s1 = e.Current.Value.Find(x => x.IndexOf(c) >= 0);
                            e.Current.Value.Remove(s1);
                            int index = s1.IndexOf(c);
                            s1 = s1.Remove(index, 1);
                            s1 = s1.Insert(index, s);
                            if (s1.Length > 1 && s1.IndexOf("λ") >= 0) s1 = s1.Remove(s1.IndexOf("λ"), 1);
                            e.Current.Value.Add(s1);
                        }
                    rules.Remove(c);
                    Noterms = Noterms.Remove(Noterms.IndexOf(c), 1);
                }
            }
            return flag;
        }

        private static string CToS(char c) { return c.ToString();  }

        private struct tempRule
        {
            public tempRule(string left, char symbol, string right)
            {
                this.left = left;
                this.symbol = symbol;
                this.right = right;
            }
            public string left;
            public char symbol;
            public string right;
        }

        private TKA LLToNKA()
        {
            List<tempRule> temprules;
            List<int> finish;
            List<string> states;
            GramToNKA(out temprules, out finish, out states, false);

            if (!states.Contains("")) states.Add(""); else
                finish.Remove(states.IndexOf(""));

            foreach (tempRule r in temprules.FindAll(x => finish.Contains(states.IndexOf(x.left))))
                temprules.Add(new tempRule("", r.symbol, r.right));
            temprules.RemoveAll(x => finish.Contains(states.IndexOf(x.left)));
            states.RemoveAll(x => finish.Contains(states.IndexOf(x)));
            for (int i = 0; i < temprules.Count; i++)
            {
                if (states.IndexOf(temprules[i].right) < 0)
                {
                    temprules.Add(new tempRule(temprules[i].left, temprules[i].symbol, ""));
                    temprules.RemoveAt(i);
                    i--;
                }
            }

            TNKA NKA = new TNKA(states.Count, Terms);
            finish = new List<int>();
            if (states.IndexOf(Start.ToString()) == -1) finish.Add(states.IndexOf("")); else
                finish.Add(states.IndexOf(Start.ToString()));
            NKA.setFinishStates(finish);
            NKA.setStartState(states.IndexOf(""));
            NKA.setName("НКА из " + Name);
            foreach (tempRule r in temprules)
                NKA.addRule(states.IndexOf(r.left), states.IndexOf(r.right), r.symbol);
            return NKA;
        }

        private void GramToNKA(out List<tempRule> temprules, out List<int> finish, out List<string> states, bool N)
        {
            temprules = new List<tempRule>();
            finish = new List<int>();
            states = new List<string>(rules.Keys.ToList().ConvertAll(new Converter<char, string>(CToS)));
            TRules.Enumerator e = rules.GetEnumerator();
            bool existsF = false;
            while (e.MoveNext())
            {
                foreach (string s in e.Current.Value)
                {
                    if (s == "λ")
                    {
                        if (!finish.Contains(states.IndexOf(e.Current.Key.ToString())))
                            finish.Add(states.IndexOf(e.Current.Key.ToString()));
                    }
                    else
                    if (s.Length == 1)
                    {
                        if (N) temprules.Add(new tempRule(e.Current.Key.ToString(), s[0], ""));
                        else
                            temprules.Add(new tempRule("", s.Last(), e.Current.Key.ToString()));
                        existsF = true;
                    }
                    else
                    {
                        string newState = N ? s.Remove(0, 1) : s.Remove(s.Length - 1, 1);
                        if (!states.Contains(newState)) states.Add(newState);
                        if (N) temprules.Add(new tempRule(e.Current.Key.ToString(), s[0], newState));
                        else
                            temprules.Add(new tempRule(newState, s.Last(), e.Current.Key.ToString()));
                    }
                }
            }
            int index = rules.Keys.Count;
            while (index < states.Count)
            {
                string curState = states[index];
                if (curState.Length == 1)
                {
                    if (N) temprules.Add(new tempRule(curState, curState[0], ""));
                    else
                        temprules.Add(new tempRule("", curState.Last(), curState));
                    existsF = true;
                }
                else
                {
                    string newState = N ? curState.Remove(0, 1) : curState.Remove(curState.Length - 1, 1);
                    if (!states.Contains(newState)) states.Add(newState);
                    if (N) temprules.Add(new tempRule(curState, curState[0], newState));
                    else
                        temprules.Add(new tempRule(newState, curState.Last(), curState));
                }
                index++;
            }
            if (existsF)
            {
                states.Add("");
                finish.Add(states.Count - 1);
            }
        }

        private TKA PLToNKA()
        {

            List<tempRule> temprules;
            List<int> finish;
            List<string> states;
            GramToNKA(out temprules, out finish, out states, true);

            TNKA NKA = new TNKA(states.Count, Terms);
            NKA.setFinishStates(finish);
            NKA.setStartState(states.IndexOf(Start.ToString()));
            NKA.setName("НКА из " + Name);
            foreach (tempRule r in temprules)
                NKA.addRule(states.IndexOf(r.left), states.IndexOf(r.right), r.symbol);
            return NKA;
        }

        public TKA ConvertToNKA()
        {
            if (type == TType.LL) return LLToNKA(); else return PLToNKA();
        }

        public TKA ConvertToDKA()
        {
            TKA DKA = ConvertToNKA();
            DKA = ((TNKA)DKA).ConvertToDKA();
            DKA.setName("ДКА из " + Name);
            return DKA;
        }

        public override void SaveToFile(StreamWriter f)
        {
            f.WriteLine("[RG-Start]");
            f.WriteLine("[Type] " + (type == TType.LL ? "LL" : "PL"));
            f.WriteLine("[Name] " + Name);
            f.WriteLine("[Noterms] " + Noterms);
            f.WriteLine("[Terms] " + Terms);
            f.WriteLine("[Start] " + Start);
            TRules.Enumerator e = rules.GetEnumerator();
            while (e.MoveNext())
                foreach (string s in e.Current.Value)
                    f.WriteLine("[Rule] " + e.Current.Key.ToString() + " " + s);
            f.WriteLine("[RG-Finish]");
        }

        public override void LoadFromFile(StreamReader f)
        {
            string s;
            while ((s = f.ReadLine()) != "[RG-Finish]")
            {
                string value = s.Remove(0, s.IndexOf(' ') + 1);
                if (s.StartsWith("[Type]")) type = value == "LL" ? TType.LL : TType.PL;
                else
                    if (s.StartsWith("[Name]")) Name = value;
                else
                    if (s.StartsWith("[Noterms]")) Noterms = value;
                else
                    if (s.StartsWith("[Terms]")) Terms = value;
                else
                    if (s.StartsWith("[Start]")) Start = value[0];
                else
                    if (s.StartsWith("[Rule]")) addRule(value[0], value.Remove(0, 2));
            }
        }

        public void GenerateChains(object input)
        {
            Main.genChainsStruct p = (Main.genChainsStruct)input;
            List<Main.generatingChains>[] chains = new List<Main.generatingChains>[p.maxLength + 1];
            for (int i = 0; i < p.maxLength + 1; i++) chains[i] = new List<Main.generatingChains>();
            int count = 0;

            chains[0].Add(new Main.generatingChains(Start.ToString(), Start.ToString()));
            for (int i = 0; i < p.maxLength + 1; i++) {
                foreach (Main.generatingChains s in chains[i])
                {
                    char Noterm = s.chain.ToList().Find(x => Noterms.IndexOf(x) >= 0);
                    foreach (string r in rules[Noterm])
                    {
                        int index = s.chain.IndexOf(Noterm);
                        string newChain = s.chain.Remove(index, 1);
                        if (r != "λ") newChain = newChain.Insert(index, r);
                        if (r.ToList().Exists(x => Noterms.IndexOf(x) >= 0))
                        {
                            if (newChain.Length <= p.maxLength + 1)
                                if (!chains[newChain.Length-1].Exists(x => x.chain == newChain))
                                    chains[newChain.Length - 1].Add(new Main.generatingChains(newChain, s.path + "→" + newChain));
                        } else
                        {
                            if (newChain.Length >= p.minLength && newChain.Length <= p.maxLength)
                            {
                                if (!p.lstOut.Items.Contains(newChain))
                                {
                                    p.chains.Add(new Main.generatingChains(newChain, s.path + "→" + newChain));
                                    p.lstOut.Invoke((MethodInvoker)(() => p.lstOut.Items.Add(newChain)));
                                    count++;
                                    p.lblCount.Invoke((MethodInvoker)(() => p.lblCount.Text = count.ToString()));
                                }
                            }
                        }
                    }
                }
            }

            p.btnStart.Invoke((MethodInvoker)(() => p.btnStart.Enabled = true));
            p.btnStop.Invoke((MethodInvoker)(() => p.btnStop.Enabled = false));
        }
    }
}
