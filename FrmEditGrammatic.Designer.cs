﻿namespace RegLangDiplom
{
    partial class FrmEditGrammatic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNoterm = new System.Windows.Forms.Label();
            this.lblTerm = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.lblArrow = new System.Windows.Forms.Label();
            this.txtNoterm = new System.Windows.Forms.TextBox();
            this.txtLine = new System.Windows.Forms.TextBox();
            this.txtSymbol = new System.Windows.Forms.TextBox();
            this.txtTerm = new System.Windows.Forms.TextBox();
            this.cbStart = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.topPanel = new System.Windows.Forms.Panel();
            this.lstChains = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.topPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNoterm
            // 
            this.lblNoterm.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblNoterm.Location = new System.Drawing.Point(4, 9);
            this.lblNoterm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNoterm.Name = "lblNoterm";
            this.lblNoterm.Size = new System.Drawing.Size(102, 22);
            this.lblNoterm.TabIndex = 0;
            this.lblNoterm.Text = "Нетерминалы";
            this.lblNoterm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTerm
            // 
            this.lblTerm.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTerm.Location = new System.Drawing.Point(4, 39);
            this.lblTerm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTerm.Name = "lblTerm";
            this.lblTerm.Size = new System.Drawing.Size(87, 22);
            this.lblTerm.TabIndex = 1;
            this.lblTerm.Text = "Терминалы";
            this.lblTerm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStart
            // 
            this.lblStart.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStart.Location = new System.Drawing.Point(4, 67);
            this.lblStart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(149, 27);
            this.lblStart.TabIndex = 2;
            this.lblStart.Text = "Целевой символ";
            this.lblStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblArrow
            // 
            this.lblArrow.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblArrow.Location = new System.Drawing.Point(47, 101);
            this.lblArrow.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblArrow.Name = "lblArrow";
            this.lblArrow.Size = new System.Drawing.Size(34, 29);
            this.lblArrow.TabIndex = 3;
            this.lblArrow.Text = "→";
            // 
            // txtNoterm
            // 
            this.txtNoterm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtNoterm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNoterm.Location = new System.Drawing.Point(110, 10);
            this.txtNoterm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNoterm.MaxLength = 32000;
            this.txtNoterm.Name = "txtNoterm";
            this.txtNoterm.Size = new System.Drawing.Size(125, 22);
            this.txtNoterm.TabIndex = 4;
            this.txtNoterm.TextChanged += new System.EventHandler(this.txtNoterm_TextChanged);
            // 
            // txtLine
            // 
            this.txtLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLine.Location = new System.Drawing.Point(88, 107);
            this.txtLine.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtLine.MaxLength = 32000;
            this.txtLine.Name = "txtLine";
            this.txtLine.Size = new System.Drawing.Size(147, 22);
            this.txtLine.TabIndex = 6;
            // 
            // txtSymbol
            // 
            this.txtSymbol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtSymbol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSymbol.Location = new System.Drawing.Point(7, 107);
            this.txtSymbol.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSymbol.MaxLength = 1;
            this.txtSymbol.Name = "txtSymbol";
            this.txtSymbol.Size = new System.Drawing.Size(33, 22);
            this.txtSymbol.TabIndex = 7;
            // 
            // txtTerm
            // 
            this.txtTerm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtTerm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTerm.Location = new System.Drawing.Point(98, 41);
            this.txtTerm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTerm.MaxLength = 32000;
            this.txtTerm.Name = "txtTerm";
            this.txtTerm.Size = new System.Drawing.Size(137, 22);
            this.txtTerm.TabIndex = 8;
            this.txtTerm.TextChanged += new System.EventHandler(this.txtTerm_TextChanged);
            // 
            // cbStart
            // 
            this.cbStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cbStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbStart.FormattingEnabled = true;
            this.cbStart.Location = new System.Drawing.Point(159, 69);
            this.cbStart.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbStart.Name = "cbStart";
            this.cbStart.Size = new System.Drawing.Size(76, 24);
            this.cbStart.TabIndex = 9;
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Location = new System.Drawing.Point(7, 134);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(228, 26);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Добавить правило";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.topPanel.Controls.Add(this.lblNoterm);
            this.topPanel.Controls.Add(this.lblTerm);
            this.topPanel.Controls.Add(this.cbStart);
            this.topPanel.Controls.Add(this.lblStart);
            this.topPanel.Controls.Add(this.txtTerm);
            this.topPanel.Controls.Add(this.txtNoterm);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(242, 100);
            this.topPanel.TabIndex = 11;
            // 
            // lstChains
            // 
            this.lstChains.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lstChains.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstChains.FormattingEnabled = true;
            this.lstChains.ItemHeight = 16;
            this.lstChains.Location = new System.Drawing.Point(7, 168);
            this.lstChains.Name = "lstChains";
            this.lstChains.Size = new System.Drawing.Size(228, 130);
            this.lstChains.TabIndex = 12;
            this.lstChains.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lstChains_KeyDown);
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(7, 305);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(113, 26);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGray;
            this.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(120, 305);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(115, 26);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // FrmEditGrammatic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(242, 338);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lstChains);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtSymbol);
            this.Controls.Add(this.txtLine);
            this.Controls.Add(this.lblArrow);
            this.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(258, 377);
            this.MinimumSize = new System.Drawing.Size(258, 343);
            this.Name = "FrmEditGrammatic";
            this.ShowIcon = false;
            this.Text = "Регулярная грамматика";
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNoterm;
        private System.Windows.Forms.Label lblTerm;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label lblArrow;
        private System.Windows.Forms.TextBox txtNoterm;
        private System.Windows.Forms.TextBox txtLine;
        private System.Windows.Forms.TextBox txtSymbol;
        private System.Windows.Forms.TextBox txtTerm;
        private System.Windows.Forms.ComboBox cbStart;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.ListBox lstChains;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}