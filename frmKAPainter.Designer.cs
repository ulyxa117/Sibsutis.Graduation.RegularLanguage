﻿namespace RegLangDiplom
{
    partial class frmKAPainter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // frmKAPainter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 371);
            this.Name = "frmKAPainter";
            this.ShowIcon = false;
            this.Text = "Граф КА";
            this.Load += new System.EventHandler(this.frmKAPainter_Load);
            this.ResizeEnd += new System.EventHandler(this.frmKAPainter_ResizeEnd);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmKAPainter_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmKAPainter_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmKAPainter_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}