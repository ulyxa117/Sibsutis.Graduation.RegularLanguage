﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegLangDiplom
{
    public abstract class TKA : TConstruction
    {
        public enum TType {DKA, NKA};
        protected TType type;
        protected string Alphabet;
        protected int CountStates;
        protected int StartState;
        protected List<int> FinishStates;
        public TKAPainter Painter;
        protected List<int>[,] Table;

        abstract public override string getTypeString();
        abstract public string CheckLine(string chain);

        public List<int>[,] getTable()
        {
            List<int>[,] t = new List<int>[CountStates, Alphabet.Length];
            for (int i = 0; i < CountStates; i++)
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    t[i, j] = new List<int>();
                    foreach (int k in Table[i, j]) t[i, j].Add(k);
                }
            return t;
        }

        protected string getKonf(int q, string chain)
        {
            return "{q" + q.ToString() + "," + (chain.Length == 0 ? "λ" : chain) + "}";
        }

        public override string getString()
        {
            int[] count = new int[Alphabet.Length];
            for (int i = 0; i < Alphabet.Length; i++)
            {
                int max = 1;
                for (int j = 0; j < CountStates; j++)
                    if (max < Table[j, i].Count) max = Table[j, i].Count();
                count[i] = max;
            }
            string str = "      ";
            for (int i = 0; i < Alphabet.Length; i++)
            {
                str += "  " + Alphabet[i] + " ";
                for (int j = 1; j < count[i]; j++) str += "    ";
            }
            for (int i = 0; i < CountStates; i++)
            {
                str += Environment.NewLine;
                if (StartState == i && FinishStates.Contains(i)) str += "*→";
                else
                    if (FinishStates.Contains(i)) str += " *";
                else
                    if (StartState == i) str += " →";
                else str += "  ";
                str += "q" + (i > 9 ? i.ToString() : i.ToString() + " ") + " ";
                for (int j = 0; j < Alphabet.Length; j++)
                {
                    if (Table[i, j].Count == 0) str += " ---";
                    else
                        foreach (int k in Table[i, j])
                        {
                            if (Table[i, j].First() == k) str += " "; else str += ",";
                            str += "q" + k.ToString() + (k > 9 ? "" : " ");
                        }
                    int c = Table[i, j].Count;
                    if (c == 0) c = 1;
                    for (int k = c; k < count[j]; k++) str += "    ";
                }
            }
            return str;
        }

        public void addRule(int q0, int q1, char c)
        {
            Table[q0, Alphabet.IndexOf(c)].Add(q1);
            Painter = new TKAPainter(this);
        }

        public override TConstruction Copy()
        {
            TKA newKA;
            if (type == TType.NKA) newKA = new TNKA(CountStates, Alphabet); else
                newKA = new TDKA(CountStates, Alphabet);
            newKA.setStartState(StartState);
            newKA.setFinishStates(FinishStates);
            for (int i = 0; i < CountStates; i++)
                for (int j = 0; j < Alphabet.Length; j++)
                    foreach (int k in Table[i, j]) newKA.addRule(i, k, Alphabet[j]);
            newKA.setName(Name);
            return newKA;
        }

        public TKA(int countStates, string alphabet)
        {
            CountStates = countStates;
            Alphabet = alphabet;
            Table = new List<int>[CountStates, Alphabet.Length];
            for (int i = 0; i < CountStates; i++)
                for (int j = 0; j < Alphabet.Length; j++)
                    Table[i, j] = new List<int>();
            Painter = new TKAPainter(this);
        }

        public void setStartState(int state)
        {
            StartState = state;
            Painter = new TKAPainter(this);
        }

        public void setFinishStates(List<int> finishStates)
        {
            FinishStates = new List<int>();
            foreach (int i in finishStates) FinishStates.Add(i);
            Painter = new TKAPainter(this);
        }

        public int getCountStates()
        {
            return CountStates;
        }

        public int getStartState()
        {
            return StartState;
        }

        public List<int> getFinishStates()
        {
            return FinishStates;
        }

        public string getAlphabet()
        {
            return Alphabet;
        }

        public TType getTypeKA()
        {
            return type;
        }

        public override void SaveToFile(StreamWriter f)
        {
            f.WriteLine("[KA-Start]");
            f.WriteLine("[Type] " + (type == TType.DKA ? "DKA" : "NKA"));
            f.WriteLine("[Name] " + Name);
            f.WriteLine("[CountStates] " + CountStates.ToString());
            f.WriteLine("[Alphabet] " + Alphabet);
            f.WriteLine("[StartState] " + StartState.ToString());
            f.Write("[FinishStates]");
            foreach (int i in FinishStates) f.Write(" " + i.ToString());
            f.WriteLine("");
            for (int i = 0; i < CountStates; i++)
                for (int j = 0; j < Alphabet.Length; j++)
                    foreach (int k in Table[i,j])
                        f.WriteLine("[Rule] " + i.ToString() + " " + k.ToString() + " " + Alphabet[j].ToString());
            f.WriteLine("[KA-Finish]");
        }

        public override void LoadFromFile(StreamReader f)
        {
            string s;
            while ((s = f.ReadLine()) != "[KA-Finish]")
            {
                string value = s.Remove(0, s.IndexOf(' ') + 1);
                if (s.StartsWith("[Name]")) Name = value;
                else
                    if (s.StartsWith("[CountStates]"))
                {
                    CountStates = Convert.ToInt32(value);
                    Table = new List<int>[CountStates, Alphabet.Length];
                    for (int i = 0; i < CountStates; i++)
                        for (int j = 0; j < Alphabet.Length; j++)
                            Table[i, j] = new List<int>();
                }
                else
                    if (s.StartsWith("[Alphabet]"))
                {
                    Alphabet = value;
                    Table = new List<int>[CountStates, Alphabet.Length];
                    for (int i = 0; i < CountStates; i++)
                        for (int j = 0; j < Alphabet.Length; j++)
                            Table[i, j] = new List<int>();
                }
                else
                    if (s.StartsWith("[StartState]")) StartState = Convert.ToInt32(value);
                else
                    if (s.StartsWith("[FinishStates]"))
                {
                    FinishStates = new List<int>();
                    foreach (string fs in value.Split(' '))
                        FinishStates.Add(Convert.ToInt32(fs));
                }
                else
                    if (s.StartsWith("[Rule]"))
                {
                    string[] v = value.Split(' ');
                    addRule(Convert.ToInt32(v[0]), Convert.ToInt32(v[1]), v[2][0]);
                }
            }
        }
    }
}
