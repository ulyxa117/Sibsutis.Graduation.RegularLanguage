﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegLangDiplom
{
    public abstract class TConstruction
    {
        protected string Name;
        protected string Type;
        abstract public TConstruction Copy();
        abstract public string getTypeString();
        abstract public string getString();
        abstract public void SaveToFile(StreamWriter f);
        abstract public void LoadFromFile(StreamReader f);

        public string getName()
        {
            return Name;
        }
        
        public void setName(string name)
        {
            Name = name;
        }
    }
}
