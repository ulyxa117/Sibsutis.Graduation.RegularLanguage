﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegLangDiplom
{
    public partial class frmKAPainter : Form
    {
        public frmKAPainter()
        {
            InitializeComponent();
        }

        TKAPainter Painter;
        Graphics g;
        SolidBrush brushState = new SolidBrush(Color.LightGray);
        SolidBrush brushText = new SolidBrush(Color.Black);
        SolidBrush brushStartState = new SolidBrush(Color.White);
        Pen penState = new Pen(Color.Black, 2);
        Pen penLine = new Pen(Color.Black, 2);
        Font font = new Font("Consolas", 11);
        bool down = false;
        int num = 0, x0, y0;

        public void setData(TKAPainter Painter)
        {
            Width = Painter.width;
            Height = Painter.height;
            this.Painter = Painter;
            repaint();
        }

        private void drawState(TKAPainter.state q)
        {
            if (!q.start) g.FillEllipse(brushState, q.x - 15, q.y - 15, 30, 30); else
                g.FillEllipse(brushStartState, q.x - 15, q.y - 15, 30, 30);
            g.DrawEllipse(penState, q.x - 15, q.y - 15, 30, 30);
            if (q.finish) g.DrawEllipse(penState, q.x - 18, q.y - 18, 36, 36);
            g.DrawString("q" + q.n.ToString(), font, brushText, new Point(q.x - 13, q.y - 9));
        }

        private void drawLine(TKAPainter.line line)
        {
            Point[] p;
            if (line.q0 == line.q1) // петля
            {
                p = new Point[4];
                p[0].X = p[3].X = Painter.states[line.q0].x;
                p[0].Y = p[3].Y = Painter.states[line.q0].y;
                double vx = p[0].X - Painter.width / 2;
                double vy = p[0].Y - Painter.height / 2;
                double dist = Math.Sqrt(Math.Pow(vx, 2) + Math.Pow(vy, 2));
                vx /= dist;
                vy /= dist;
                p[1].X = p[0].X + Convert.ToInt32(vx * 50 + vy * 10);
                p[1].Y = p[0].Y + Convert.ToInt32(vy * 50 - vx * 10);
                p[2].X = p[0].X + Convert.ToInt32(vx * 50 - vy * 10);
                p[2].Y = p[0].Y + Convert.ToInt32(vy * 50 + vx * 10);
                double vx1 = p[2].X - p[3].X;
                double vy1 = p[2].Y - p[3].Y;
                dist = Math.Sqrt(Math.Pow(vx1, 2) + Math.Pow(vy1, 2));
                vx1 = vx1 / dist * 20;
                vy1 = vy1 / dist * 20;
                p[3].X += Convert.ToInt32(vx1);
                p[3].Y += Convert.ToInt32(vy1);
                g.DrawCurve(penLine, p);
                g.DrawString(line.c, font, brushText, 
                    new Point(Convert.ToInt32(p[0].X + vx * 60), Convert.ToInt32(p[0].Y + vy * 60)));
            } else
            {
                p = new Point[3];
                p[0].X = Painter.states[line.q0].x;
                p[0].Y = Painter.states[line.q0].y;
                p[2].X = Painter.states[line.q1].x;
                p[2].Y = Painter.states[line.q1].y;
                double vx = p[2].X - p[0].X;
                double vy = p[2].Y - p[0].Y;
                double dist = Math.Sqrt(Math.Pow(vx, 2) + Math.Pow(vy, 2));
                vx /= dist;
                vy /= dist;
                p[1].X = Convert.ToInt32((p[0].X + p[2].X) / 2 + vy * 10);
                p[1].Y = Convert.ToInt32((p[0].Y + p[2].Y) / 2 - vx * 10);
                double vx1 = p[1].X - p[2].X;
                double vy1 = p[1].Y - p[2].Y;
                dist = Math.Sqrt(Math.Pow(vx1, 2) + Math.Pow(vy1, 2));
                vx1 = vx1 / dist * 20;
                vy1 = vy1 / dist * 20;
                p[2].X += Convert.ToInt32(vx1);
                p[2].Y += Convert.ToInt32(vy1);
                g.DrawCurve(penLine, p);
                g.DrawString(line.c, font, brushText,
                    new Point(Convert.ToInt32(p[1].X + Math.Abs(vy) * 10), Convert.ToInt32(p[1].Y - Math.Abs(vx) * 10)));
            }
        }

        private void frmKAPainter_Load(object sender, EventArgs e)
        {
            g = CreateGraphics();
            penLine.CustomEndCap = new AdjustableArrowCap(5, 5);
        }

        private void repaint()
        {
            try
            {
                g.Clear(BackColor);
                foreach (TKAPainter.line l in Painter.lines)
                    drawLine(l);
                for (int i = 0; i < Painter.countStates; i++)
                    drawState(Painter.states[i]);
            }
            catch { }
        }

        private void frmKAPainter_MouseUp(object sender, MouseEventArgs e)
        {
            down = false;
            int xmax = 0, ymax = 0;
            for (int i = 0; i < Painter.countStates; i++)
            {
                if (Painter.states[i].x > xmax) xmax = Painter.states[i].x;
                if (Painter.states[i].y > ymax) ymax = Painter.states[i].y;
            }
            Painter.height = ymax + 100;
            Painter.width = xmax + 100;
        }

        private void frmKAPainter_MouseMove(object sender, MouseEventArgs e)
        {
            if (down)
            {
                Painter.states[num].x = e.X - x0;
                Painter.states[num].y = e.Y - y0;
                repaint();
            }
        } 

        private void frmKAPainter_ResizeEnd(object sender, EventArgs e)
        {
            g = CreateGraphics();
            repaint();
        }

        private void frmKAPainter_MouseDown(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < Painter.countStates; i++) 
                if (Math.Sqrt(Math.Pow(e.X - Painter.states[i].x, 2) + Math.Pow(e.Y - Painter.states[i].y, 2)) <= 30)
                {
                    down = true;
                    num = i;
                    x0 = e.X - Painter.states[i].x;
                    y0 = e.Y - Painter.states[i].y;
                    break;
                }
        }
    }
}
