﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegLangDiplom
{
    public class TKAPainter
    {
        public struct state
        {
            public int n;
            public int x;
            public int y;
            public bool start;
            public bool finish;
        }
        public struct line
        {
            public int q0;
            public int q1;
            public string c;
        }
        public int width, height;
        public state[] states;
        public List<line> lines;
        public int countStates;

        public TKAPainter(TKA KA)
        {
            countStates = KA.getCountStates();
            double angle = 0;
            states = new state[KA.getCountStates()];
            double r = 150 * KA.getCountStates() / (2 * Math.PI);
            if (r < 150) r = 150;
            for (int i = 0; i < KA.getCountStates(); i++)
            {
                states[i] = new state();
                states[i].n = i;
                states[i].x = Convert.ToInt32(Math.Cos(angle) * r + 50 + r);
                states[i].y = Convert.ToInt32(Math.Sin(angle) * r + 50 + r);
                states[i].start = KA.getStartState() == i;
                if (KA.getFinishStates() != null) states[i].finish = KA.getFinishStates().Contains(i);
                angle += 2 * Math.PI / KA.getCountStates();
            }
            lines = new List<line>();
            List<int>[,] t = KA.getTable();
            for (int i = 0; i < KA.getCountStates(); i++)
                for (int j = 0; j < KA.getAlphabet().Length; j++)
                    foreach (int k in t[i,j])
                    {
                        line l = new line();
                        l.q0 = i;
                        l.q1 = k;
                        l.c = KA.getAlphabet()[j].ToString();
                        if (lines.Exists(x => x.q0 == l.q0 && x.q1 == l.q1))
                        {
                            line l1 = lines.Find(x => x.q0 == l.q0 && x.q1 == l.q1);
                            l.c += "," + l1.c;
                            lines.Remove(l1);
                        }
                        lines.Add(l);
                    }
            width = Convert.ToInt32(2 * r + 100);
            height = Convert.ToInt32(2 * r + 100);
        }
    }
}
