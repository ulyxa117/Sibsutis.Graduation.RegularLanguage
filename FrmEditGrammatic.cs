﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegLangDiplom
{
    public partial class FrmEditGrammatic : Form
    {
        public FrmEditGrammatic()
        {
            InitializeComponent();
        }

        public TGrammatic gram;

        public void setData(TGrammatic gram)
        {
            txtTerm.Text = gram.getTerms();
            txtNoterm.Text = gram.getNoterms();
            cbStart.Text = gram.getStart().ToString();
            Dictionary<char, List<string>>.Enumerator e = gram.getRules().GetEnumerator();
            while (e.MoveNext())
                foreach (string s in e.Current.Value)
                    lstChains.Items.Add(e.Current.Key + "→" + s);
        }

        private void txtNoterm_TextChanged(object sender, EventArgs e)
        {
            char start = new char();
            if (cbStart.Text.Length > 0) start = cbStart.Text[0];
            cbStart.Items.Clear();
            IEnumerable<char> lst = txtNoterm.Text.Distinct().Except(txtTerm.Text);
            txtNoterm.Text = new string(lst.ToArray());
            foreach (char c in txtNoterm.Text) cbStart.Items.Add(c);
            if (cbStart.Items.Contains(start)) cbStart.Text = start.ToString();
        }

        private void txtTerm_TextChanged(object sender, EventArgs e)
        {
            IEnumerable<char> lst = txtTerm.Text.Distinct().Except(txtNoterm.Text);
            txtTerm.Text = new string(lst.ToArray());
        }

        private int checkRule(string symbol, string line)
        {
            if (symbol.Length == 0) return 1;
            if (txtNoterm.Text.IndexOf(symbol[0]) < 0) return 2;

            int countTerm = 0, countNoterm = 0;
            bool flag = false;
            foreach (char c in line)
            {
                if (txtTerm.Text.IndexOf(c) >= 0) countTerm++; else
                if (txtNoterm.Text.IndexOf(c) >= 0) countNoterm++; else
                {
                    flag = true;
                    break;
                }
            }
            if (flag) return 3;

            if (countNoterm > 1) return 4;
            if (countNoterm == 1 && txtTerm.Text.IndexOf(line.First()) >= 0 && txtTerm.Text.IndexOf(line.Last()) >= 0) return 5;
            return 0;
        }

        private string getErrorString(int code)
        {
            switch (code)
            {
                case 1: return "Не задана левая часть правила";
                case 2: return "В левой части правила должен быть нетерминальный символ";
                case 3: return "В правой части правила присутствуют символы, не принадлежащие ни к терминальному, ни к нетерминальному алфавиту";
                case 4: return "В правой части правила может быть не более одного нетерминала";
                case 5: return "Нетерминал в правой части правила не может находиться в середине строки";
                default: return "";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string rule = txtSymbol.Text + "→" + (txtLine.Text.Length == 0 ? "λ" : txtLine.Text);
            int code;
            if (!lstChains.Items.Contains(rule)) code = checkRule(txtSymbol.Text, txtLine.Text);
                else return;
            
            if (code > 0)
            {
                string error = getErrorString(code);
                MessageBox.Show(error, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            lstChains.Items.Add(rule);
            txtLine.Text = "";
        }

        private bool LL(string line)
        {
            if (line.Length < 2) return true;
            if (txtNoterm.Text.Contains(line[line.Length - 1])) return false;
            return true;
        }

        private bool PL(string line)
        {
            if (line.Length < 2) return true;
            if (txtNoterm.Text.Contains(line[0])) return false;
            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            gram = new TGrammatic();
            gram.setNoterms(txtNoterm.Text);
            gram.setTerms(txtTerm.Text);

            int i = 0, code = 0;
            bool left = true, right = true;
            foreach (string s in lstChains.Items)
            {
                string[] parts = s.Split('→');
                gram.addRule(parts[0][0], parts[1]);

                left = left & LL(parts[1]);
                right = right & PL(parts[1]);
                if (!left && !right)
                {
                    MessageBox.Show("Грамматика не является леволинейной и праволинейной", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    code = 6;
                    break;
                }
                if (parts[1] == "λ") parts[1] = "";
                code = checkRule(parts[0], parts[1]);
                if (code > 0)
                {
                    MessageBox.Show(getErrorString(code), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                }
                i++;
            }
            if (code > 0)
            {
                lstChains.SelectedIndex = i;
                return;
            }
            if (cbStart.Text.Length == 0)
            {
                MessageBox.Show("Не задан целевой символ грамматики", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (gram.getRules().Keys.Count != txtNoterm.Text.Length)
            {
                MessageBox.Show("Необходимо чтобы для каждого нетерминала было создано хотя бы одно правило", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            gram.setStart(cbStart.Text[0]);
            gram.setType(left ? TGrammatic.TType.LL : TGrammatic.TType.PL);
            DialogResult = DialogResult.OK;
        }

        private void lstChains_KeyDown(object sender, KeyEventArgs e)
        {
            if (lstChains.SelectedIndex >= 0 && e.KeyCode == Keys.Delete)
                lstChains.Items.RemoveAt(lstChains.SelectedIndex);
        }
    }
}
